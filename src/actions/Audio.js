import config from '../config'
import axios from 'axios';

export const initAudio = () => dispatch => {

    dispatch({
        type: "INIT_AUDIO"
    });

};

export const pauseAndPlayAudio = () => dispatch => {

    dispatch({
        type: "PAUSE_AND_PLAY_AUDIO"
    });

};

export const playAudio = () => dispatch => {

    dispatch({
        type: "PLAY_AUDIO"
    });

};