import config from '../config'
import axios from 'axios';
import Pusher from 'pusher-js';
import {newOrderModalShow, getIncomingOrders, getAcceptedOrders, getInDeliveryOrders, addRightNowIncomingOrders} from './Orders';

export const signIn = (data) => dispatch => {

    if(navigator.onLine){
        dispatch({
            type: "ACTIVE_LOADER"
        });

        var isConnectionProblem = true;
    
        axios({
            method: 'post',
            url: `${config.url}/partner/login`,
            data: {
                login: data.login,
                password: data.password
            }
        })
        .then(function (response) {
            isConnectionProblem = false;
            
            dispatch({
                type: "DEACTIVE_LOADER"
            });
            
            if(response.status == 200){
                let dataForDispatch = {
                    type: "",
                    code: response.data.code,
                    message: response.data.message,
                }
    
                if(response.data.code == 200){
                    dataForDispatch.type = "SIGNED_IN"
                    dataForDispatch.authorization = response.headers.authorization
                }
                else{
                    dataForDispatch.type = "SIGNED_OUT"
                }
                dispatch(dataForDispatch);
            }
        });

        setTimeout(() => {
            if(isConnectionProblem){
                dispatch({
                    type: "DEACTIVE_LOADER"
                });

                dispatch({
                    type: "ACTIVE_MINIMODAL",
                    text: "Internet bağlantısı yoxdur"
                });

                setTimeout(() => {
                    dispatch({
                        type: "DEACTIVE_MINIMODAL",
                        text: ""
                    });
                }, 4000);
            }
        }, 10000);
    }
    else{
        dispatch({
            type: "ACTIVE_MINIMODAL",
            text: "Internet bağlantısı yoxdur"
        });

        setTimeout(() => {
            dispatch({
                type: "DEACTIVE_MINIMODAL",
                text: ""
            });
        }, 4000);
    }
};

export const signInOffline = (data) => dispatch => {

    dispatch({
        type: "SIGNED_IN",
        authorization: data.authorization
    });

};

export const getProfile = (data) => dispatch => {

    if(navigator.onLine){
        dispatch({
            type: "ACTIVE_LOADER"
        });

        var isConnectionProblem = true;
    
        axios({
            method: 'post',
            url: `${config.url}/partner/profile`,
            headers: {
                Authorization: data.authorization,
                'Content-Language': 'en'
            }
        })
        .then(function (response) {
            isConnectionProblem = false;
            
            dispatch({
                type: "DEACTIVE_LOADER"
            });
            
            if(response.status == 200){
                let dataForDispatch = {
                    type: "",
                    code: response.data.code,
                    message: response.data.message,
                }
    
                if(response.data.code == 200){
                    dispatch({
                        type: "GET_PROFILE",
                        data: response.data
                    });
    
                    dispatch({
                        type: response.data.data.online ? "ACTIVE_LINE_STATUS" : "DEACTIVE_LINE_STATUS"
                    });
    
                    dispatch({
                        type: response.data.data.rush_time ? "ACTIVE_RUSH_TIME_STATUS" : "DEACTIVE_RUSH_TIME_STATUS"
                    });
    
                    dispatch(initPusher({channel: response.data.data.channel, authorization: data.authorization}))
                }
                else{
                    if(response.data.code == 465 || response.data.code == 466) dataForDispatch.type = "SIGNED_OUT"
                }
                dispatch(dataForDispatch);
            }
        });

        setTimeout(() => {
            if(isConnectionProblem){
                dispatch({
                    type: "DEACTIVE_LOADER"
                });

                dispatch({
                    type: "ACTIVE_MINIMODAL",
                    text: "Internet bağlantısı yoxdur"
                });

                setTimeout(() => {
                    dispatch({
                        type: "DEACTIVE_MINIMODAL",
                        text: ""
                    });
                }, 4000);
            }
        }, 10000);
    }
    else{
        dispatch({
            type: "ACTIVE_MINIMODAL",
            text: "Internet bağlantısı yoxdur"
        });

        setTimeout(() => {
            dispatch({
                type: "DEACTIVE_MINIMODAL",
                text: ""
            });
        }, 4000);
    }

};

export const initPusher = (mainData) => dispatch => {

    const pusher = new Pusher("475e23577db47fab28c6", {
      cluster: "ap2",
      authEndpoint: "https://voop.az/pusher/pusher_auth.php",
      forceTLS: true
    });

    var isConnected = true;

    pusher.connection.bind('error', function( err ) {
      console.log(err);
    });

    pusher.connection.bind('disconnected', function() {
        console.log("disconnected")
    })

    var channel = pusher.subscribe(`private-${mainData.channel}`);

    channel.bind('client-new_order', (data) => {

      dispatch(newOrderModalShow({
          status: true,
          inComingCount: data.inComingCount
      }))

      dispatch(addRightNowIncomingOrders({
          inComingCount: data.inComingCount,
          orderId: data.order_id,
          arrivedTime: data.arrived_time,
      }))
    });

    channel.bind('client-courier_assigned', (data) => {

        dispatch({
            type: "UPDATE_ACCEPTED_ORDERS_COURIER",
            data: data
        })
    });

    channel.bind('client-move-to-delivery', (data) => {
        console.log("client-move-to-delivery")
        dispatch(getAcceptedOrders({authorization: mainData.authorization}))
        dispatch(getInDeliveryOrders({authorization: mainData.authorization}))
    });

    channel.bind('client-user_cancel_order', (data) => {
        console.log("client-user_cancel_order")
        dispatch(getIncomingOrders({authorization: mainData.authorization}))
        dispatch(getAcceptedOrders({authorization: mainData.authorization}))
        dispatch(getInDeliveryOrders({authorization: mainData.authorization}))
    });

    setInterval(() => {
        if(channel["subscribed"] && !isConnected){
            isConnected = true;
            alert("Bağlantı bərpa olundu")
        }
        else{
            if(!channel["subscribed"] && isConnected){
                isConnected = false;
                alert("Bağlantıda problem var, ola bilsin sifarişlər qəbul edə bilməyəsiniz")

                channel = pusher.subscribe(`private-${mainData.channel}`);
            }
        }
    }, 10000);

};

export const signOut = (data) => dispatch => {

    if(navigator.onLine){
        dispatch({
            type: "ACTIVE_LOADER"
        });

        var isConnectionProblem = true;
    
        axios({
            method: 'post',
            url: `${config.url}/partner/logout`,
            headers: {
                Authorization: data.authorization
            }
        })
        .then(function (response) {
            isConnectionProblem = false;

            dispatch({
                type: "DEACTIVE_LOADER"
            });
            
            if(response.status == 200){
                let dataForDispatch = {
                    type: "",
                    code: response.data.code,
                    message: response.data.message,
                }
    
                if(response.data.code == 200){
                    dataForDispatch.type = "SIGNED_OUT"
                    dataForDispatch.authorization = response.headers.authorization
                }
                else{
                    dataForDispatch.type = "SIGNED_OUT"
                }
                dispatch(dataForDispatch);
            }
        });

        setTimeout(() => {
            if(isConnectionProblem){
                dispatch({
                    type: "DEACTIVE_LOADER"
                });

                dispatch({
                    type: "ACTIVE_MINIMODAL",
                    text: "Internet bağlantısı yoxdur"
                });

                setTimeout(() => {
                    dispatch({
                        type: "DEACTIVE_MINIMODAL",
                        text: ""
                    });
                }, 4000);
            }
        }, 10000);
    }
    else{
        dispatch({
            type: "ACTIVE_MINIMODAL",
            text: "Internet bağlantısı yoxdur"
        });

        setTimeout(() => {
            dispatch({
                type: "DEACTIVE_MINIMODAL",
                text: ""
            });
        }, 4000);
    }

};
