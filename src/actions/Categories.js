import config from '../config'
import axios from 'axios';

export const getCategories = (data) => dispatch => {

    if(navigator.onLine){
        dispatch({
            type: "ACTIVE_LOADER"
        });

        var isConnectionProblem = true;
    
        axios({
            method: 'post',
            url: `${config.url}/partner/categories`,
            headers: {
                Authorization: data.authorization,
                'Content-Language': 'en'
            }
        })
        .then(function (response) {
            isConnectionProblem = false;
    
            dispatch({
                type: "DEACTIVE_LOADER"
            });
            
            if(response.status == 200){
                let dataForDispatch = {
                    type: "",
                    code: response.data.code,
                    message: response.data.message,
                }
    
                if(response.data.code == 200 || response.data.code == 201){
    
                    dispatch({
                        type: "EMPTY_CATEGORIES"
                    });
                    
                    dispatch({
                        type: "GET_CATEGORIES",
                        data: response.data
                    });
                }
                else{
                    if(response.data.code == 465 || response.data.code == 466) dataForDispatch.type = "SIGNED_OUT"
                }
                dispatch(dataForDispatch);
            }
        });

        setTimeout(() => {
            if(isConnectionProblem){
                dispatch({
                    type: "DEACTIVE_LOADER"
                });

                dispatch({
                    type: "ACTIVE_MINIMODAL",
                    text: "Internet bağlantısı yoxdur"
                });

                setTimeout(() => {
                    dispatch({
                        type: "DEACTIVE_MINIMODAL",
                        text: ""
                    });
                }, 4000);
            }
        }, 10000);
    }
    else{
        dispatch({
            type: "ACTIVE_MINIMODAL",
            text: "Internet bağlantısı yoxdur"
        });

        setTimeout(() => {
            dispatch({
                type: "DEACTIVE_MINIMODAL",
                text: ""
            });
        }, 4000);
    }

};