import config from '../config'
import axios from 'axios';

export const getIncomingOrders = (data) => dispatch => {

    if(navigator.onLine){
        dispatch({
            type: "ACTIVE_LOADER"
        });

        var isConnectionProblem = true;
    
        axios({
            method: 'post',
            url: `${config.url}/partner/inComing`,
            headers: {
                Authorization: data.authorization,
                'Content-Language': 'en'
            }
        })
        .then(function (response) {
            isConnectionProblem = false;
    
            dispatch({
                type: "DEACTIVE_LOADER"
            });
            
            if(response.status == 200){
                let dataForDispatch = {
                    type: "",
                    code: response.data.code,
                    message: response.data.message,
                }
    
                if(response.data.code == 200 || response.data.code == 201){
    
                    dispatch({
                        type: "EMPTY_INCOMING_ORDERS"
                    });

                    dispatch({
                        type: "GET_INCOMING_ORDERS",
                        data: response.data
                    });
                }
                else{
                    if(response.data.code == 465 || response.data.code == 466) dataForDispatch.type = "SIGNED_OUT"
                }
                dispatch(dataForDispatch);
            }
        });

        setTimeout(() => {
            if(isConnectionProblem){
                dispatch({
                    type: "DEACTIVE_LOADER"
                });

                dispatch({
                    type: "ACTIVE_MINIMODAL",
                    text: "Internet bağlantısı yoxdur"
                });

                setTimeout(() => {
                    dispatch({
                        type: "DEACTIVE_MINIMODAL",
                        text: ""
                    });
                }, 4000);
            }
        }, 10000);
    }
    else{
        dispatch({
            type: "ACTIVE_MINIMODAL",
            text: "Internet bağlantısı yoxdur"
        });

        setTimeout(() => {
            dispatch({
                type: "DEACTIVE_MINIMODAL",
                text: ""
            });
        }, 4000);
    }

};

export const getAcceptedOrders = (data) => dispatch => {

    if(navigator.onLine){
        dispatch({
            type: "ACTIVE_LOADER"
        });

        var isConnectionProblem = true;
    
        axios({
            method: 'post',
            url: `${config.url}/partner/accepted`,
            headers: {
                Authorization: data.authorization,
                'Content-Language': 'en'
            }
        })
        .then(function (response) {
            isConnectionProblem = false;
    
            dispatch({
                type: "DEACTIVE_LOADER"
            });
            
            if(response.status == 200){
                let dataForDispatch = {
                    type: "",
                    code: response.data.code,
                    message: response.data.message,
                }
    
                if(response.data.code == 200 || response.data.code == 201){
    
                    dispatch({
                        type: "EMPTY_ACCEPTED_ORDERS"
                    });

                    dispatch({
                        type: "GET_ACCEPTED_ORDERS",
                        data: response.data
                    });
                }
                else{
                    if(response.data.code == 465 || response.data.code == 466) dataForDispatch.type = "SIGNED_OUT"
                }
                dispatch(dataForDispatch);
            }
            else{
            }
        });

        setTimeout(() => {
            if(isConnectionProblem){
                dispatch({
                    type: "DEACTIVE_LOADER"
                });

                dispatch({
                    type: "ACTIVE_MINIMODAL",
                    text: "Internet bağlantısı yoxdur"
                });

                setTimeout(() => {
                    dispatch({
                        type: "DEACTIVE_MINIMODAL",
                        text: ""
                    });
                }, 4000);
            }
        }, 10000);
    }
    else{
        dispatch({
            type: "ACTIVE_MINIMODAL",
            text: "Internet bağlantısı yoxdur"
        });

        setTimeout(() => {
            dispatch({
                type: "DEACTIVE_MINIMODAL",
                text: ""
            });
        }, 4000);
    }

};

export const getInDeliveryOrders = (data) => dispatch => {

    if(navigator.onLine){
        dispatch({
            type: "ACTIVE_LOADER"
        });

        var isConnectionProblem = true;
    
        axios({
            method: 'post',
            url: `${config.url}/partner/inDelivery`,
            headers: {
                Authorization: data.authorization,
                'Content-Language': 'en'
            }
        })
        .then(function (response) {
            isConnectionProblem = false;
    
            dispatch({
                type: "DEACTIVE_LOADER"
            });
            
            if(response.status == 200){
                let dataForDispatch = {
                    type: "",
                    code: response.data.code,
                    message: response.data.message,
                }
    
                if(response.data.code == 200 || response.data.code == 201){
    
                    dispatch({
                        type: "EMPTY_INDELIVERY_ORDERS"
                    });

                    dispatch({
                        type: "GET_INDELIVERY_ORDERS",
                        data: response.data
                    });
                }
                else{
                    if(response.data.code == 465 || response.data.code == 466) dataForDispatch.type = "SIGNED_OUT"
                }
                dispatch(dataForDispatch);
            }
        });

        setTimeout(() => {
            if(isConnectionProblem){
                dispatch({
                    type: "DEACTIVE_LOADER"
                });

                dispatch({
                    type: "ACTIVE_MINIMODAL",
                    text: "Internet bağlantısı yoxdur"
                });

                setTimeout(() => {
                    dispatch({
                        type: "DEACTIVE_MINIMODAL",
                        text: ""
                    });
                }, 4000);
            }
        }, 10000);
    }
    else{
        dispatch({
            type: "ACTIVE_MINIMODAL",
            text: "Internet bağlantısı yoxdur"
        });

        setTimeout(() => {
            dispatch({
                type: "DEACTIVE_MINIMODAL",
                text: ""
            });
        }, 4000);
    }

};

export const updateOrderStatus = (data) => dispatch => {

    if(navigator.onLine){
        dispatch({
            type: "ACTIVE_LOADER"
        });
    
        let bodyData = {
            order_id: data.orderId,
            order_status: data.status,
        }

        if(data.cancel_reason) bodyData.cancel_reason = data.cancel_reason;
    
        if(data.hasOwnProperty("cookingTime")) bodyData.cooking_time = data.cookingTime;

        var isConnectionProblem = true;
    
        axios({
            method: 'post',
            url: `${config.url}/partner/changeOrderStatus`,
            headers: {
                Authorization: data.authorization,
                'Content-Language': 'en'
            },
            data: bodyData
        })
        .then(function (response) {
            isConnectionProblem = false;
    
            dispatch({
                type: "DEACTIVE_LOADER"
            });
            
            if(response.status == 200){
                let dataForDispatch = {
                    type: "",
                    code: response.data.code,
                    message: response.data.message,
                }
    
                if(response.data.code == 200){
                    if(data.status == 4 || data.status == 5){
                        dispatch(getIncomingOrders(data))
                        dispatch(getAcceptedOrders(data))
                    }
                    else{
                        dispatch({
                            type: "UPDATE_ACCEPTED_ORDERS_STATUS",
                            data: data
                        });
                    }
                }
                else{
                    if(response.data.code == 465 || response.data.code == 466) dataForDispatch.type = "SIGNED_OUT"
                }
                dispatch(dataForDispatch);
            }
        });

        setTimeout(() => {
            if(isConnectionProblem){
                dispatch({
                    type: "DEACTIVE_LOADER"
                });

                dispatch({
                    type: "ACTIVE_MINIMODAL",
                    text: "Internet bağlantısı yoxdur"
                });

                setTimeout(() => {
                    dispatch({
                        type: "DEACTIVE_MINIMODAL",
                        text: ""
                    });
                }, 4000);
            }
        }, 10000);
    }
    else{
        dispatch({
            type: "ACTIVE_MINIMODAL",
            text: "Internet bağlantısı yoxdur"
        });

        setTimeout(() => {
            dispatch({
                type: "DEACTIVE_MINIMODAL",
                text: ""
            });
        }, 4000);
    }

};

export const updateOrderCookingTime = (data) => dispatch => {

    dispatch({
        type: "UPDATE_INCOMING_ORDERS_COOKING_TIME",
        data: data
    });

};
export const updateOrderAttention = (data) => dispatch => {

    dispatch({
        type: `UPDATE_${data.orderType}_ORDERS_ATTENTION`,
        data: data
    });

};

export const newOrderModalShow = (data) => dispatch => {

    dispatch({
        type: "ACTIVE_NEW_ORDER_MODAL",
        data: {
            status: true,
            inComingCount: data.inComingCount
        }
    });

};

export const newOrderModalHide = (data) => dispatch => {

    dispatch(getIncomingOrders({
        authorization: data.authorization
    }));

    dispatch({
        type: "DEACTIVE_NEW_ORDER_MODAL"
    });

    dispatch({
        type: "EMPTY_RIGHT_NOW_INCOMING_ORDERS"
    });

    if(window.location.href != "/orders") data.history.push("/orders")

};

export const addRightNowIncomingOrders = (data) => dispatch => {

    dispatch({
        type: "ADD_RIGHT_NOW_INCOMING_ORDERS",
        data: {
            inComingCount: data.inComingCount,
            orderId: data.orderId,
            arrivedTime: data.arrivedTime,
        }
    });

};

export const removeOneRightNowIncomingOrders = (data) => dispatch => {

    dispatch({
        type: "REMOVE_ONE_RIGHT_NOW_INCOMING_ORDER",
        data: {
            orderId: data.orderId
        }
    });

};

export const emptyIncomingOrders = () => dispatch => {

    dispatch({
        type: "EMPTY_INCOMING_ORDERS"
    });

};

export const emptyAcceptedOrders = () => dispatch => {

    dispatch({
        type: "EMPTY_ACCEPTED_ORDERS"
    });

};

export const emptyInDeliveryOrders = () => dispatch => {

    dispatch({
        type: "EMPTY_INDELIVERY_ORDERS"
    });

};

export const emptyRightNowIncomingOrders = () => dispatch => {

    dispatch({
        type: "EMPTY_RIGHT_NOW_INCOMING_ORDERS",
        data: {}
    });

};