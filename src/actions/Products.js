import config from '../config'
import axios from 'axios';

export const getProducts = (data) => dispatch => {

    if(navigator.onLine){

        let bodyData = {}
        if(data.hasOwnProperty("categoryId")) bodyData.category = data.categoryId
    
        dispatch({
            type: "ACTIVE_LOADER"
        });

        var isConnectionProblem = true;
    
        axios({
            method: 'post',
            url: `${config.url}/partner/products`,
            headers: {
                Authorization: data.authorization,
                'Content-Language': 'en'
            },
            data: bodyData
        })
        .then(function (response) {
            isConnectionProblem = false;
    
            dispatch({
                type: "DEACTIVE_LOADER"
            });
            
            if(response.status == 200){
                let dataForDispatch = {
                    type: "",
                    code: response.data.code,
                    message: response.data.message,
                }
    
                if(response.data.code == 200 || response.data.code == 201){
    
                    dispatch({
                        type: "EMPTY_PRODUCTS"
                    });
                    
                    for(let i = 0; i < response.data.data.length; i++){
                        response.data.data[i].extraFieldsShow = false;
                    }
                    dispatch({
                        type: "GET_PRODUCTS",
                        data: response.data
                    });
                }
                else{
                    if(response.data.code == 465 || response.data.code == 466) dataForDispatch.type = "SIGNED_OUT"
                }
                dispatch(dataForDispatch);
            }
        });

        setTimeout(() => {
            if(isConnectionProblem){
                dispatch({
                    type: "DEACTIVE_LOADER"
                });

                dispatch({
                    type: "ACTIVE_MINIMODAL",
                    text: "Internet bağlantısı yoxdur"
                });

                setTimeout(() => {
                    dispatch({
                        type: "DEACTIVE_MINIMODAL",
                        text: ""
                    });
                }, 4000);
            }
        }, 10000);
    }
    else{
        dispatch({
            type: "ACTIVE_MINIMODAL",
            text: "Internet bağlantısı yoxdur"
        });

        setTimeout(() => {
            dispatch({
                type: "DEACTIVE_MINIMODAL",
                text: ""
            });
        }, 4000);
    }

};

export const updateProductStatus = (data) => dispatch => {

    if(navigator.onLine){

        dispatch({
            type: "ACTIVE_LOADER"
        });
    
        let bodyData = {
            product_id: data.productId,
            status: data.status,
        }
    
        if(data.hasOwnProperty("extraFieldKey")) bodyData.extra_field_id = data.extraFieldKey;

        var isConnectionProblem = true;
    
        axios({
            method: 'post',
            url: `${config.url}/partner/productStatus`,
            headers: {
                Authorization: data.authorization,
                'Content-Language': 'en'
            },
            data: bodyData
        })
        .then(function (response) {
            isConnectionProblem = false;
    
            dispatch({
                type: "DEACTIVE_LOADER"
            });
            
            if(response.status == 200){
                let dataForDispatch = {
                    type: "",
                    code: response.data.code,
                    message: response.data.message,
                }
    
                if(response.data.code == 200){
                    dispatch({
                        type: "UPDATE_PRODUCT_STATUS",
                        data: data
                    });
                }
                else{
                    if(response.data.code == 465 || response.data.code == 466) dataForDispatch.type = "SIGNED_OUT"
                }
                dispatch(dataForDispatch);
            }
        });

        setTimeout(() => {
            if(isConnectionProblem){
                dispatch({
                    type: "DEACTIVE_LOADER"
                });

                dispatch({
                    type: "ACTIVE_MINIMODAL",
                    text: "Internet bağlantısı yoxdur"
                });

                setTimeout(() => {
                    dispatch({
                        type: "DEACTIVE_MINIMODAL",
                        text: ""
                    });
                }, 4000);
            }
        }, 10000);
    }
    else{
        dispatch({
            type: "ACTIVE_MINIMODAL",
            text: "Internet bağlantısı yoxdur"
        });

        setTimeout(() => {
            dispatch({
                type: "DEACTIVE_MINIMODAL",
                text: ""
            });
        }, 4000);
    }

};

export const updateProductViewStatus = (data) => dispatch => {

    dispatch({
        type: "UPDATE_PRODUCT_VIEW_STATUS",
        data: data
    });

};