/* global window document */

import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './components/App';
// import './static/css/style.sass';

const AppClient = () => (
    <App />
);

window.onload = () => {
  ReactDOM.render(<AppClient />, document.getElementById('main'));
};
