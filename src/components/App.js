import React from 'react';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';

import store from '../store'

import {Provider} from 'react-redux'

import Layout from './Layout';

import Videos from './pages/Videos';
import MenuChange from './pages/MenuChange';
import Login from './pages/Login';
import History from './pages/History';
import Orders from './pages/Orders';

import NotFoundPage from './pages/NotFoundPage';

export const App = () => (
  
  <Provider store={store} >
    <Router basename="/partner">
      <Layout>
        <Switch>
          <Route exact path="/orders" component={Orders} />
          {/* <Route exact path="/videos" component={Videos} /> */}
          <Route exact path="/menu-change" component={MenuChange} />
          <Route exact path="/(login|)" component={Login} />
          <Route exact path="/history" component={History} />

          <Route component={NotFoundPage} />
        </Switch>
      </Layout>
    </Router>
  </Provider>
    
);

export default App;