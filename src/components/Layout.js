import React, {Component} from 'react';
import { Link, withRouter} from 'react-router-dom';
import {connect} from 'react-redux'
import Cookies from 'universal-cookie';

import Scripts from './Scripts';

import Header from './containers/Header'
import Loader from './containers/Loader';
import NewOrderModal from './containers/NewOrderModal';
import MiniModal from './containers/MiniModal';

import Login from './pages/Login';

import {signInOffline} from '../actions/Auth';
import config from '../config';
import Intercom from 'react-intercom';

import {initAudio, pauseAndPlayAudio, playAudio} from '../actions/Audio';
import {removeOneRightNowIncomingOrders} from '../actions/Orders';
import {newOrderModalHide, updateOrderStatus} from '../actions/Orders';


export class Layout extends Component {

  constructor(props){
    super(props)
  }

  componentWillMount(){
    if(this.props.auth.signed === 0){
      if((new Cookies()).get('authorization')) this.props.signInOffline({authorization: (new Cookies()).get('authorization')})
      else{
        this.props.history.push("/login")
        if(window.location.pathname != "/partner/login") window.location.href = "/partner/login"
      }
    }

    this.props.initAudio()
    this.forgottenRightNowIncommingOrders()
  }

  componentDidMount(){
    Scripts.init();
  }

  forgottenRightNowIncommingOrders(){

    let now = new Date();


    if(this.props.rightNowIncomingOrders.data){
      this.props.rightNowIncomingOrders.data.map((item, index) => {

        let arrivalTime = new Date(item.arrivedTime.split('-').join('/'));
        
        let diffOfDates = now - arrivalTime;

        if(diffOfDates >= (this.props.profile.data.max_waiting_time * 60 * 1000)){
          if(this.props.rightNowIncomingOrders.data.length <= 1) {
            this.props.newOrderModalHide({history: this.props.history, authorization: this.props.auth.authorization})

            this.props.audio.loop = false;
            this.props.audio.src = `music/${config.newOrderMelody}`;
            this.props.audio.pause()
          }
          this.props.updateOrderStatus({
            authorization: this.props.auth.authorization,
            orderId: item.orderId,
            status: 5,
            cancel_reason: "time_is_over"
          })
          this.props.removeOneRightNowIncomingOrders({orderId: item.orderId})
        }

      })
    }

    setTimeout(() => {
      this.forgottenRightNowIncommingOrders()
    }, 5000);
  }

  render(){

    var user = {}

    if(this.props.profile.data){
      user = {
        user_id: `partner-${this.props.profile.data.id}`,
        email: this.props.profile.data.email,
        name: this.props.profile.data.name
      };
    }

    return this.props.auth.signed === 1 ? (
      <div>
        <Header />

        {this.props.children}
        <Loader />
        <MiniModal />

        <NewOrderModal />
        <Intercom appID="z3pwk2dy" { ...user } />
        
      </div>
    ) : (
      <div>
        {/* {this.props.children} */}
        <Login />
        <Loader />
      </div>
    )
  }
};

function mapStateToProps(state){
  return {
    auth: state.auth,
    profile: state.profile,
    rightNowIncomingOrders: state.rightNowIncomingOrders,
    audio: state.audio
  }
}

export default withRouter(connect(
  mapStateToProps, 
  dispatch => ({
    signInOffline: (data) => {
      dispatch(signInOffline(data));
    },
    initAudio: () => {
      dispatch(initAudio());
    },
    removeOneRightNowIncomingOrders: (data) => {
      dispatch(removeOneRightNowIncomingOrders(data));
    },
    newOrderModalHide: (data) => {
      dispatch(newOrderModalHide(data));
    },
    updateOrderStatus: (data) => {
      dispatch(updateOrderStatus(data));
    },
  })
)(Layout));
