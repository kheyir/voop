import config from '../config'
import Cookies from 'universal-cookie';
import {TweenLite, Power2} from "gsap/all";
import $ from 'jquery'

export const stringify = (obj_from_json) => {
    if( (typeof obj_from_json !== "object" && !Array.isArray(obj_from_json) )|| obj_from_json == null){
        // not an object, stringify using native function
        return JSON.stringify(obj_from_json);
    }
    // Implements recursive object serialization according to JSON spec
    // but without quotes around the keys.
    let props = Object
        .keys(obj_from_json)
        .map(key => Array.isArray(obj_from_json) ? `${stringify(obj_from_json[key])}` : `"${key}":${stringify(obj_from_json[key])}`)
        .join(",");
        
    return Array.isArray(obj_from_json) ? `[${props}]` : `{${props}}`;
}

export const init = () => {
    
    initialization();
}

export const refreshToken = (func) => {
    $.ajax({
        method: "GET",
        headers: {
            'access_token': (new Cookies()).get('token'),
            'request_number': Scripts.getRandomInt(100000, 999999)
        },
        url: `${config.url}/fin/api/aas/refreshToken`,

    })
        .success((res) => {

            if(res.code == 0){
                (new Cookies()).set('token', res.token, { path: '/' });
                func();
            }
        });
}

export const validationText = {
    isEmpty: "Sahəni doldurun!"
}

export const alertMessages = {
    success: "Əməliyyat müvəffəqiyyətlə yerinə yetirildi!",
    danger: "Əməliyyat səhvlərə görə yerinə yetirilmədi!"
}

export const mesages = {
    RELATED_DATA_EXIST: "Silmək istədiyiniz element başqa məlumata bağlıdır!"
}

export const getRandomInt = (min, max) => {
    return Math.floor(Math.random() * (max - min)) + min;
}

export const dateWithoutMilliseconds = (date) => {
    return date.split('.')[0];
}

export const checkboxOnChange = (e) => {

    $(e.target).next().removeClass("form-group-simple__checkmark_checked")
    $(e.target).next().removeClass("form-group-simple__checkmark_not-full-checked")

    if($(e.target).hasClass("form-group-simple__global-check-child")){

        var dataSelectorChild = $(e.target).data("selector-child")

        var fullChecked = true
        var checkCount = 0
        $("[data-selector-child='"+dataSelectorChild+"']").each((index, item) => {
            if(!$(item).is(":checked")) fullChecked = false
            else checkCount++
        })

        $("[data-selector='"+dataSelectorChild+"']").next().removeClass("form-group-simple__checkmark_checked")
        $("[data-selector='"+dataSelectorChild+"']").next().removeClass("form-group-simple__checkmark_not-full-checked")
        $("[data-selector='"+dataSelectorChild+"']").prop("checked", true)
        if(fullChecked) {
            $("[data-selector='"+dataSelectorChild+"']").next().addClass("form-group-simple__checkmark_checked")
        }
        else {
            if(checkCount > 0 && checkCount < $("[data-selector-child='"+dataSelectorChild+"']").length) $("[data-selector='"+dataSelectorChild+"']").next().addClass("form-group-simple__checkmark_not-full-checked")
        }
    }
    else{
        if($(e.target).hasClass("form-group-simple__global-check-parent")){
    
            var dataSelector = $(e.target).data("selector")
            
            $("[data-selector-child='"+dataSelector+"']").each((index, item) => {
                if($(e.target).is(":checked")) $(item).prop("checked", true)
                else $(item).prop("checked", false)
            })
    
            if($(e.target).is(":checked")) $(e.target).next().addClass("form-group-simple__checkmark_checked")
            else $(e.target).next().removeClass("form-group-simple__checkmark_checked")
        }
    }

    if($(e.target).is(":checked")) $(e.target).next().addClass("form-group-simple__checkmark_checked")
    else $(e.target).next().removeClass("form-group-simple__checkmark_checked")
    
}

export const initialization = () => {

    $(document).delegate(".nav-tabs__link", "click", (e) => {

        $(e.target).closest(".nav-tabs").find(".nav-tabs__link").each(function(index, item){

            $(item).closest(".nav-tabs__item").removeClass("active")

            $($(item).data("tab")).css("display", "none")
        })

        $(e.target).closest(".nav-tabs__item").addClass("active")

        let linkTag = $(e.target)

        if(!linkTag.hasClass(".nav-tabs__link")) linkTag = linkTag.closest(".nav-tabs__link")

        $(linkTag.data("tab")).css("display", "block")
    })

    // $("#teams-tab").tab('show')

    // $("#all-country").tab('show')

    // $("#members-tab").tab('show')

    // $('.select2-football-1').select2();

    $(".langs-block__selected").on("click", function(){
        if($(".langs-block__lang-list").is(":visible")) $(".langs-block__lang-list").hide()
        else $(".langs-block__lang-list").show()
    })

    $(document).delegate(".select2-section-football-1", "click", function(){
        $(".select2-results__options").find("[aria-selected='true']").css("background-color", "#F6F6F6")
        $(".select2-results__options").find("[aria-selected='true']").css("color", "#000")
    })


    var touchMoveOff = function (e){
        e.preventDefault()
    }

    function navMenuActive(){
        $("#open-menu").hide()
        $("#open-menu").removeClass("nav-mobile-menu-button")
        $("#close-menu").show()
        $(".mobile-nav-section").show()
    
        //window.scrollTo(0, 0);
        var navMobile = document.getElementById("mobile-nav-block");
        TweenLite.to(navMobile, 0.5, {right: 0, ease:Power2.easeInOut});
        $("html").css("overflow-y", "hidden")
        //$("#nav-mobile").css("position", "fixed")
        //$("#nav-mobile").css("z-index", "999")
    
        window.addEventListener('touchmove', touchMoveOff, { passive: false });
    }
    
    function navMenuDisactive(){
        $("#open-menu").show()
        $("#open-menu").addClass("nav-mobile-menu-button")
        $("#close-menu").hide()
    
        var navMobile = document.getElementById("mobile-nav-block");
        var right = window.innerWidth > 478 ? -375 : -window.innerWidth
        TweenLite.to(navMobile, 0.5, {right: right, ease:Power2.easeInOut, onComplete: function(){$(".mobile-nav-section").hide()} });
        $("html").css("overflow-y", "auto")
        //$("#nav-mobile").css("position", "unset")
        //$("#nav-mobile").css("z-index", "auto")
    
        window.removeEventListener('touchmove', touchMoveOff, { passive: false });
    }

    $("#open-menu").on("click", navMenuActive)
    $("#close-menu").on("click", navMenuDisactive)

    if($("#menu").is(":visible")){
        $(".search-input").css("width", $("#menu").width())
        $(".search-input").css("margin-right", "65px")
    }
    else{
        $(".search-input").css("width", "calc(100% - "+($("#icons-block").width() + 20) +"px)")
    }

    //$(".search-input").css("width", $("#menu").is(":visible") ? $("#menu").width() : "calc(100% - "+$("#icons-block").width()+"px)")
    $("#open-search").on("click", function(){
        $("#menu").hide()
        $(".search-input").show()

        $("#open-search").hide()
        $("#close-search").show()
    })

    $("#close-search").on("click", function(){
        $("#menu").show()
        $(".search-input").hide()

        $("#open-search").show()
        $("#close-search").hide()
    })

    $("body").delegate(".filter-open-button", "click", function(e){
        if($(e.target).hasClass("active")){
            $(e.target).next().hide()
            $(e.target).removeClass("active")
        } 
        else{
            $(e.target).next().show()
            $(e.target).addClass("active")
        }
    })

    $("body").delegate(".table-game-video tr", "click", function(){
        $(this).parent().find("tr").css("display", "none")
        $(this).css("display", "table-row")
        $(this).closest(".table-game-video-block").find("#main-goal-videos").show()
        $(this).closest(".table-game-video-block").find(".table-game-video-button").css("display", "flex")
        $(this).closest(".table-game-video-block").find(".table-game-video__text").hide()
    })

    $("body").delegate(".table-game-video-button", "click", function(){
        $(this).closest(".table-game-video-block").find(".table-game-video").first().find("tr").css("display", "table-row")
        $(this).closest(".table-game-video-block").find("#main-goal-videos").hide()
        $(this).css("display", "none")
        $(this).closest(".table-game-video-block").find(".table-game-video__text").show()
    })

    $(window).resize(ligasButtons)

}

export const ligasButtons = () => {
    $(".ligas-list").each(function(index, item){
        if($(item).width() > $(item).find(".owl-stage").last().width()){
            $(item).closest(".slider-wrapper").find(".slider-button-block").hide()
        }
        else{
            $(item).closest(".slider-wrapper").find(".slider-button-block").show()
        }
    })
}

export const videosHeightChange = () => {
    let header = $(".header").outerHeight(true)
    let videosHead = $(".videos-head").outerHeight(true)

    $(".videos-list").css("height", "calc(100vh - "+(header + videosHead)+"px)")
}

export const menuChangeProductsHeightChange = () => {
    let header = $(".header").outerHeight(true)
    let menuChangePadding = parseInt($(".menu-change").css("padding-top"))
    let menuChangeMarginBottom = parseInt($(".menu-change-list").css("margin-bottom"))
    let textBlock = $(".text-block").outerHeight(true)

    $(".menu-change-list").css("height", "calc(100vh - "+(header + menuChangePadding + menuChangeMarginBottom + textBlock)+"px)")
}

export const menuChangeCategoriesHeightChange = () => {
    let header = $(".header").outerHeight(true)
    let menuChangePadding = parseInt($(".menu-change").css("padding-top"))
    let menuChangeFilterTitle = $(".menu-change__filter-title").outerHeight(true)

    $(".menu-change-filter-list").css("height", "calc(100vh - "+(header + menuChangePadding + menuChangeFilterTitle)+"px)")
}

export const historyListsBlockHeightChange = () => {
    let header = $(".header").outerHeight(true)
    let menuChangePadding = parseInt($(".history").css("padding-top"))
    let historyListsBlock = parseInt($(".history-lists-block").css("margin-bottom"))
    let historyRightFooterHeight = $(".history__right-footer").outerHeight(true)

    $(".history-lists-block").css("height", "calc(100vh - "+(header + menuChangePadding + historyListsBlock + historyRightFooterHeight)+"px)")
}

export const ordersListBlocksHeightChange = () => {
    let header = $(".header").outerHeight(true)
    let menuChangePaddingTop = parseInt($(".orders").css("padding-top"))
    let menuChangePaddingBottom = parseInt($(".orders").css("padding-bottom"))
    let ordersIncomingTitleHeight = $(".orders-incoming__title").outerHeight(true)
    let ordersAcceptedTitleHeight = $(".orders-accepted__title").outerHeight(true)

    $(".orders-incoming-list-block, .orders-accepted-list-block").css("height", "calc(100vh - "+(header + menuChangePaddingTop + menuChangePaddingBottom + Math.max(ordersIncomingTitleHeight, ordersAcceptedTitleHeight))+"px)")
}

export const roadOrdersHeightChange = () => {
    let header = $(".header").outerHeight(true)
    let menuChangePaddingTop = parseInt($(".orders").css("padding-top"))
    let menuChangePaddingBottom = parseInt($(".orders").css("padding-bottom"))
    let ordersIncomingTitleHeight = $(".orders-incoming__title").outerHeight(true)
    let ordersAcceptedTitleHeight = $(".orders-accepted__title").outerHeight(true)
    let ordersRoadHeader = $(".orders-road__header").outerHeight(true)
    let ordersRoadBodyPaddingTop = parseInt($(".orders-road__body").css("padding-top")) || 15
    let ordersRoadBodyPaddingBottom = parseInt($(".orders-road__body").css("padding-bottom")) || 15
    let ordersRoadError = $(".orders-road .error").outerHeight(true) || 0

    $(".orders-road-list").css("height", "calc(100vh - "+(header + menuChangePaddingTop + menuChangePaddingBottom + Math.max(ordersIncomingTitleHeight, ordersAcceptedTitleHeight) + ordersRoadHeader + ordersRoadBodyPaddingTop + ordersRoadBodyPaddingBottom + ordersRoadError)+"px)")
}

const playSoundNTimes = (audio, times, ended) => {
    if (times <= 0) {
        return;
    }
    var played = 0;
    audio.addEventListener("ended", function() {
        played++;
        if (played < times) audio.play();
    });
    audio.play();
}

const Scripts = {
    init,
    ligasButtons,
    videosHeightChange,
    menuChangeProductsHeightChange,
    menuChangeCategoriesHeightChange,
    historyListsBlockHeightChange,
    ordersListBlocksHeightChange,
    roadOrdersHeightChange,
    checkboxOnChange,
    stringify,
    refreshToken,
    validationText,
    alertMessages,
    mesages,
    getRandomInt,
    dateWithoutMilliseconds,
    playSoundNTimes
}

export default Scripts;