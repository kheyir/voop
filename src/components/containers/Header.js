import React, {Component} from 'react';
import { Link, withRouter} from 'react-router-dom';
import {connect} from 'react-redux'
import {getProfile, signOut} from '../../actions/Auth';
import {updateLineStatus} from '../../actions/LineStatus';
import {updateRushTimeStatus} from '../../actions/RushTimeStatus';

class Header extends Component{

  constructor(props){
    super(props)

    this.state = {
      chatIsOpened: false
    }
  }

  componentWillMount(){
    this.props.getProfile({
        authorization: this.props.auth.authorization
    })
  }

  handleOnChange(name, value){

    var obj = Object.assign({}, this.state);

    obj[name] = value;

    this.setState(obj)
  }

  // chatShowHide(){

  //   if(this.state.chatIsOpened){
  //     jivo_api.close()
  //   }
  //   else{
  //     jivo_api.open()
  //   }

  //   this.setState({
  //     chatIsOpened: !this.state.chatIsOpened
  //   })
  // }

  render(){
    return (
      <div className={`header${!this.props.lineStatus ? " header_offline" : ""}`}>
        <ul className="menu-list mr-10">
          <li className={`menu-list__item${this.props.location.pathname === "/orders" || this.props.location.pathname === "/" ? " active" : ""}`}>
            <Link to="/orders" className="menu-list__link">Sifarişlər</Link>
          </li>
          <li className={`menu-list__item${this.props.location.pathname === "/history" ? " active" : ""}`}>
            <Link to="/history" className="menu-list__link">Tarixçə</Link>
          </li>
          <li className={`menu-list__item${this.props.location.pathname === "/menu-change" ? " active" : ""}`}>
            <Link to="/menu-change" className="menu-list__link">Menyuda düzəliş</Link>
          </li>
          {/* <li className={`menu-list__item${this.props.location.pathname === "/videos" ? " active" : ""}`}>
            <Link to="/videos" className="menu-list__link">Videolar</Link>
          </li> */}
        </ul>

        {/* <a href="javascript:void(0)" className="link link_icon link_icon_chat ml-auto mr-20" onClick={this.chatShowHide.bind(this)}></a> */}

        <button className={`button button_icon button_icon_warning button_bor_rad_min button_white_grad button_c_green-blue ml-auto mr-10${this.props.rushTimeStatus ? " button_white_grad_pushed" : ""}`} onClick={this.props.updateRushTimeStatus.bind(this, {status: !this.props.rushTimeStatus, authorization: this.props.auth.authorization})}>Pik saatı rejimi({this.props.rushTimeStatus ? "ON" : "OFF"})</button>
        <button className="button button_icon button_icon_dot button_bor_rad_min button_white_grad button_c_green-blue mr-15" onClick={this.props.updateLineStatus.bind(this, {status: !this.props.lineStatus, authorization: this.props.auth.authorization})}>{this.props.lineStatus ? "Online" : "Offline"}</button>

        <span className="label label_white mr-5">
          <a href="javascript:void(0)" className="link link_black">{this.props.profile.data ? this.props.profile.data.name : ''}</a>

          <a href="javascript:void(0)" className="link link_icon link_icon_logout" onClick={this.props.signOut.bind(this, {authorization: this.props.auth.authorization})}></a>
        </span>
      </div>
    )
  }
}

function mapStateToProps(state){
  return {
    auth: state.auth,
    profile: state.profile,
    lineStatus: state.lineStatus,
    rushTimeStatus: state.rushTimeStatus
  }
}

export default withRouter(connect(
  mapStateToProps, 
  dispatch => ({
    getProfile: (data) => {
      dispatch(getProfile(data));
    },
    updateLineStatus: (data) => {
      dispatch(updateLineStatus(data));
    },
    updateRushTimeStatus: (data) => {
      dispatch(updateRushTimeStatus(data));
    },
    signOut: (data) => {
      dispatch(signOut(data));
    }
  })
)(Header));
