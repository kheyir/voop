import React from 'react';
import {connect} from 'react-redux'

export const Loader = (props) => (
  <div style={{display: (props.loader ? "flex" : "none"), alignItems: "center", justifyContent: "center", position: "fixed", top: 0, right: 0, bottom: 0, left: 0, zIndex: 999, backgroundColor: "#FFF", opacity: .6}}>
   <img src="images/spinner.svg" />
  </div>
)

function mapStateToProps(state){
  return {
    loader: state.loader
  }
}

export default connect(
  mapStateToProps
)(Loader);
