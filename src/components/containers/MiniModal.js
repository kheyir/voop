import React from 'react';
import {connect} from 'react-redux'

export const MiniModal = (props) => (
  props.miniModal.status && (
    <div style={{position: "fixed", bottom: 50, left: "50%", transform: "translate(-50%, 0)", borderRadius: 5, padding: "15px 20px", backgroundColor: "rgba(0, 0, 0, .7)", color: "#FFF", fontSize: 20}}>
      {props.miniModal.text}
    </div>
  )
)

function mapStateToProps(state){
  return {
    miniModal: state.miniModal
  }
}

export default connect(
  mapStateToProps
)(MiniModal);
