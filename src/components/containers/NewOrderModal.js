import React from 'react';
import { withRouter} from 'react-router-dom';
import {connect} from 'react-redux'
import ReactAudioPlayer from 'react-audio-player';
import config from '../../config'
import {newOrderModalHide} from '../../actions/Orders';

export class NewOrderModal extends React.Component{

  constructor(props){
    super(props);

    // this.audio = document.createElement("audio")
  }

  componentDidMount(){
    // this.audio.src = `music/${config.newOrderMelody}`
    // this.audio.loop = true

    console.log("componentDidMount")
  }

  componentWillUnmount(){
    console.log("componentWillUnmount")
  }

  onClick(){
    this.props.newOrderModalHide({history: this.props.history, authorization: this.props.auth.authorization})

    this.props.audio.loop = false;
    this.props.audio.pause()
  }

  render(){

    if(this.props.newOrderModal.status){
      this.props.audio.loop = true;
      this.props.audio.src = `music/${config.newOrderMelody}`;
      this.props.audio.play()
    }

    return(
      this.props.newOrderModal.status && (
        <div className="new-order" onClick={this.onClick.bind(this)}>
          <div className="new-order__main">
            <span className="new-order__count">{this.props.rightNowIncomingOrders.data.length}</span>
            <span className="new-order__main-label">YENİ SİFARİŞ</span>
          </div>

          <span className="new-order__description">Qəbul etmək üçün istənilən yerə toxunun</span>

          {/* <ReactAudioPlayer
            src={`music/${config.newOrderMelody}`}
            autoPlay={true}
            loop={true}
            volume={1.0}
          /> */}
        </div>
      )
      
    )
  }
}

function mapStateToProps(state){
  return {
    auth: state.auth,
    newOrderModal: state.newOrderModal,
    audio: state.audio,
    rightNowIncomingOrders: state.rightNowIncomingOrders,
  }
}

export default withRouter(connect(
  mapStateToProps, 
  dispatch => ({
    newOrderModalHide: (data) => {
      dispatch(newOrderModalHide(data));
    }
  })
)(NewOrderModal));