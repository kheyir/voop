import React from 'react';
import Scripts from '../Scripts';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux'
import {getHistory} from '../../actions/History'

export class History extends React.Component{

  constructor(props){
    super(props);

    this.state = {
      historyType: "product",
      historyFilter: "1"
    }
  }

  componentWillMount(){
    this.props.getHistory({
        authorization: this.props.auth.authorization,
        type: this.state.historyType,
        filter: this.state.historyFilter
    })
  }

  componentDidMount(){
    Scripts.historyListsBlockHeightChange()
  }

  viewChange(value){
    this.setState({
      historyType: value
    })

    this.props.getHistory({
      authorization: this.props.auth.authorization,
      type: value,
      filter: this.state.historyFilter
    })
  }

  filterChange(value){
    this.setState({
      historyFilter: value
    })

    this.props.getHistory({
      authorization: this.props.auth.authorization,
      type: this.state.historyType,
      filter: value
    })
  }
  
  render(){

    return(
        <div className="history">
            <div className="history__left" style={{display: this.state.historyType == "product" ? "none" : "block"}}>
              <ul className="history-filter-list">
                <li className={`history-filter-list__item${this.state.historyFilter == "1" ? " active" : ""}`}>
                  <a href="javascript:void(0)" className="history-filter-list__link" onClick={this.filterChange.bind(this, "1")}>Bu gün</a>
                </li>
                <li className={`history-filter-list__item${this.state.historyFilter == "2" ? " active" : ""}`}>
                  <a href="javascript:void(0)" className="history-filter-list__link" onClick={this.filterChange.bind(this, "2")}>Dünən</a>
                </li>
                <li className={`history-filter-list__item${this.state.historyFilter == "7" ? " active" : ""}`}>
                  <a href="javascript:void(0)" className="history-filter-list__link" onClick={this.filterChange.bind(this, "7")}>Bu həftə</a>
                </li>
                <li className={`history-filter-list__item${this.state.historyFilter == "14" ? " active" : ""}`}>
                  <a href="javascript:void(0)" className="history-filter-list__link" onClick={this.filterChange.bind(this, "14")}>Keçən həftə</a>
                </li>
                <li className={`history-filter-list__item${this.state.historyFilter == "30" ? " active" : ""}`}>
                  <a href="javascript:void(0)" className="history-filter-list__link" onClick={this.filterChange.bind(this, "30")}>Bu ay</a>
                </li>
                <li className={`history-filter-list__item${this.state.historyFilter == "60" ? " active" : ""}`}>
                  <a href="javascript:void(0)" className="history-filter-list__link" onClick={this.filterChange.bind(this, "60")}>Keçən ay</a>
                </li>
              </ul>
            </div>

            <div className="history__right mr-5">
              
              <div className="history-lists-block">
                
                {
                  this.state.historyType == "product" ? (
                    <div className="history-product-block">
                      {
                        this.props.history.data && this.props.history.data.length > 0 && (
                          <table className="history-table">
                          <thead>
                            <tr>
                              <th>MƏHSUL</th>
                              <th>QİYMƏT (₼)</th>
                              <th>MİQDAR</th>
                              <th>MƏBLƏĞ (₼)</th>
                            </tr>
                          </thead>
                          <tbody>
                            {
                              this.props.history.data.map(historyItem => (
                                <tr>
                                  <td className="td_bold">{historyItem.product_name}</td>
                                  <td>{historyItem.price}</td>
                                  <td>{historyItem.quantity}</td>
                                  <td>{historyItem.total}</td>
                                </tr>
                              ))
                            }
                          </tbody>
                        </table>
                        )
                      }
                      {
                        this.props.history.code == 201 && (
                          <span className="error error_black">{this.props.history.message}</span>
                        )
                      }
                    </div>
                  ) : (
                    <div className="history-order-block">
                      <ul className="history-order-list">
                        {
                          this.props.history.data && this.props.history.data.length > 0 && this.props.history.data.map(historyItem => (
                            <li className="history-order-list__item">
                              <div className="order">
                                <div className="order__body">
                                  <div className="order__left">
                                    <div className="order__main-info">
                                      <h1 className="order__user">{historyItem.user_name}</h1>
                                      <span className="order__date">{historyItem.date}</span>
                                    </div>
                                    {
                                      historyItem.products && historyItem.products.length > 0 && (
                                        <ul className="order-extra-list">
                                          {
                                            historyItem.products.map(productItem => (
                                              <li className="order-extra-list__item">
                                                <div className="order-extra-list__info">
                                                  <span className="order-extra-list__count">{productItem.quantity}</span>
                                                  <span className="order-extra-list__name">{productItem.name}</span>
                                                </div>
                                              </li>
                                            ))
                                          }
                                        </ul>
                                      )
                                    }
                                    <span className="order__price">₼ {historyItem.order_price}</span>
                                  </div>
                                  <div className="order__right">
                                    <span className="order__id">#{historyItem.order_id}</span>
                                    {/* <span className="order__add-text order__add-text_icon order__add-text_icon_motorcycle">Çatdırılma</span> */}
                                  </div>
                                </div>
                              </div>
                            </li>
                          ))
                        }
                      </ul>
                      {
                        this.props.history.code == 201 && (
                          <span className="error error_black">{this.props.history.message}</span>
                        )
                      }
                    </div>  
                  )
                }

              
              </div>

              <div className="history__right-footer">

                <ul className="history-nav-list">
                  <li className={`history-nav-list__item${this.state.historyType == "order" ? " active" : ""}`}>
                    <button type="button" className="history-nav-list__button" onClick={this.viewChange.bind(this, "order")}>SİFARİŞ ÜZRƏ</button>
                  </li>
                  <li className={`history-nav-list__item${this.state.historyType == "product" ? " active" : ""}`}>
                    <button type="button" className="history-nav-list__button" onClick={this.viewChange.bind(this, "product")}>MƏHSUL ÜZRƏ</button>
                  </li>
                </ul>
                
                {/* <div className="text-block ml-auto mr-20">
                  <span className="text__name">İmtina edilmiş sifarişlər</span>
                  <span className="text__value text__value_danger">₼ 10.50</span>
                </div> */}
                <div className="text-block ml-auto mr-10" style={{display: this.state.historyType == "order" ? "block" : "none"}}>
                  <span className="text__name">Ümumi gəlir</span>
                  <span className="text__value">{this.props.history.total_revenue}</span>
                </div>
              </div>

            </div>
        </div>
    )
  }
}

function mapStateToProps(state){
  return {
    auth: state.auth,
    history: state.history
  }
}

export default withRouter(connect(
  mapStateToProps, 
  dispatch => ({
    getHistory: (data) => {
      dispatch(getHistory(data));
    }
  })
)(History));