import React from 'react';
import { withRouter, Redirect} from 'react-router-dom';
import {connect} from 'react-redux'
import {signIn} from '../../actions/Auth';

export class Login extends React.Component{

  constructor(props){
    super(props);

    this.state = {
      login: "",
      password: "",

      errors: []
    }
  }

  handleOnChange(name, value){

    var obj = Object.assign({}, this.state);

    obj[name] = value;

    this.setState(obj)
  }

  validation(){
    let errors = []

    let tmpError = {login: []}
    if(this.state.login === "") tmpError.login.push("Zəhmət olmasa xananı doldurun")
    if(this.state.login.length < 3) tmpError.login.push("Xananın uzunluğu 3 simvoldan artıq olmalıdır")

    if(tmpError.login.length > 0) errors.push(tmpError)

    tmpError = {password: []}
    if(this.state.password === "") tmpError.password.push("Zəhmət olmasa xananı doldurun")
    if(this.state.password.length < 8) tmpError.password.push("Xananın uzunluğu 8 simvoldan artıq olmalıdır")

    var returnParam;
    if(tmpError.password.length > 0){
      errors.push(tmpError)
      returnParam = false;
    }
    else{
      returnParam = true;
    }

    this.setState({
      errors: errors
    })

    return returnParam

  }

  submit(){

    if(this.validation()) this.props.signIn({login: this.state.login, password: this.state.password})
  }
  
  render(){

    if (this.props.auth.signed === 1) {
      if(this.props.history.location.pathname == "/history") return <Redirect to="/history" />
      else{
        if(this.props.history.location.pathname == "/menu-change") return <Redirect to="/menu-change" />
        else return <Redirect to="/orders" />
      }
      // return <Redirect to="/orders" />
    }

    return(
        <div className="login-block">
          <div className="login-panel">
            <div className="login-panel__header">
              <h1 className="login-panel__title">Daxil ol</h1>
              <img src="images/mini_logo.png" className="login-panel__logo" />
            </div>
            <div className="login-panel__body">
              <div className={`form-group ${this.state.errors.find(item => item.hasOwnProperty("login")) ? 'form-group_has-error' : ''}`}>
                <label className="form-group__label">İstifadəçi adı</label>
                <input className="form-group__input" value={this.state.login} onChange={e => {this.handleOnChange("login", e.target.value)}} />
                {
                  this.state.errors.find(item => item.hasOwnProperty("login")) && this.state.errors.find(item => item.hasOwnProperty("login")).login.map(item => (
                    <span className="form-group__error">{item}</span>
                  ))
                }
                
              </div>
              <div className={`form-group ${this.state.errors.find(item => item.hasOwnProperty("password")) ? 'form-group_has-error' : ''}`}>
                <label className="form-group__label">Şifrə</label>
                <input type="password" className="form-group__input" value={this.state.password} onChange={e => {this.handleOnChange("password", e.target.value)}} />
                {
                  this.state.errors.find(item => item.hasOwnProperty("password")) && this.state.errors.find(item => item.hasOwnProperty("password")).password.map(item => (
                    <span className="form-group__error">{item}</span>
                  ))
                }
              </div>
            </div>
            <div className="login-panel__footer">
              {
                this.props.auth.code && this.props.auth.code !== 200 && (
                  <span className="error error_black">{this.props.auth.message}</span>
                )
              }
              <button type="button" onClick={this.submit.bind(this)} className="button button_bg_green-blue w-100 ta-center">Daxil ol</button>
            </div>
          </div>
        </div>
    )
  }
}

function mapStateToProps(state){
  return {
    auth: state.auth
  }
}

export default withRouter(connect(
  mapStateToProps, 
  dispatch => ({
    signIn: (data) => {
      dispatch(signIn(data));
    }
  })
)(Login));