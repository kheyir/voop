import React from 'react';
import Scripts from '../Scripts';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux'
import {getProducts, updateProductStatus, updateProductViewStatus} from '../../actions/Products'
import {getCategories} from '../../actions/Categories'

export class MenuChange extends React.Component{

  constructor(props){
    super(props);

    this.state = {
      selectedCategoryId: ""
    }
  }

  handleOnChange(name, value){

    var obj = Object.assign({}, this.state);

    obj[name] = value;

    this.setState(obj)

    if(name === "selectedCategoryId"){
      this.props.getProducts({
        authorization: this.props.auth.authorization,
        categoryId: value
      })
    }
  }

  componentWillMount(){
    this.props.getProducts({
        authorization: this.props.auth.authorization
    })

    this.props.getCategories({
      authorization: this.props.auth.authorization
    })
  }

  componentDidMount(){
    Scripts.menuChangeProductsHeightChange()
    Scripts.menuChangeCategoriesHeightChange()
  }
  
  render(){

    return(
        <div className="menu-change">
            <div className="menu-change__left">
              <h1 className="menu-change__filter-title">KATEQORİYALAR</h1>
              <ul className="menu-change-filter-list">
                {
                  this.props.categories.data && this.props.categories.data.length > 0 && this.props.categories.data.map(categoryItem => (
                    <li className={`menu-change-filter-list__item${this.state.selectedCategoryId == categoryItem.id ? " active" : ""}`}>
                      <a href="javascript:void(0)" className="menu-change-filter-list__link" onClick={this.handleOnChange.bind(this, "selectedCategoryId", categoryItem.id)}>{categoryItem.title}</a>
                    </li>
                  ))
                }
              </ul>
              {
                this.props.categories.code == 201 && (
                  <span className="error error_black">{this.props.categories.message}</span>
                )
              }
            </div>

            <div className="menu-change__right mr-5">
              <ul className="menu-change-list">
                {
                  this.props.products.data && this.props.products.data.length > 0 && this.props.products.data.map((productItem, productIndex) => (
                    <li className={`menu-change-list__item${productItem.extra_fields && productItem.extraFieldsShow ? " active" : ""}`} onClick={(e) => {this.props.updateProductViewStatus({productIndex: productIndex, status: !productItem.extraFieldsShow}); e.stopPropagation() }}>
                      <a href="javascript:void(0)" className="menu-change-list__link">
                        <div className="menu-change-list__title-price">
                          <h2 className="menu-change-list__title">{productItem.title}</h2>
                          <span className="menu-change-list__price">₼ {productItem.price}</span>
                        </div>
                        <div className="menu-change-list__description-button">
                          <span className="menu-change-list__description">{productItem.description}</span>
                          <button className={`button button_bor_rad_max button_pad_max button_bg_green-blue${!productItem.status ? " disabled button_border-warm" : ""}`} onClick={(e) => {this.props.updateProductStatus({productId: productItem.product_id, status: productItem.status ? "disable" : "enable", authorization: this.props.auth.authorization}); e.stopPropagation();}}>{productItem.status ? "AKTİV" : "QEYRİ-AKTİV"}</button>
                        </div>
                        {
                          productItem.extra_fields && productItem.extra_fields.length > 0 && productItem.extraFieldsShow && (
                            <ul className="menu-change-extra-list">
                              {
                                productItem.extra_fields.map((extraFieldItem, extraFieldIndex) => (
                                  <li className="menu-change-extra-list__item">
                                    <span className="menu-change-extra-list__name">{extraFieldItem.name}</span>
                                    <span className="menu-change-extra-list__price">+₼ {extraFieldItem.price}</span>
                                    <button className={`button button_bor_rad_max button_pad_max button_bg_green-blue${!extraFieldItem.status ? " disabled button_border-warm" : ""}`} onClick={(e) => {this.props.updateProductStatus({productId: productItem.product_id, extraFieldKey: extraFieldItem.extra_field_key, status: extraFieldItem.status ? "disable" : "enable", authorization: this.props.auth.authorization}); e.stopPropagation();}}>{extraFieldItem.status ? "AKTİV" : "QEYRİ-AKTİV"}</button>
                                  </li>
                                ))
                              }
                            </ul>
                          )
                        }
                        
                      </a>
                    </li>
                  ))
                }           
              </ul>

              {
                this.props.products.code == 201 && (
                  <span className="error error_black">{this.props.categories.message}</span>
                )
              }
              <div className="text-block ml-auto mr-10 mb-5">
                <span className="text__name">Cəmi məhsul çeşidi</span>
                <span className="text__value">{this.props.products.count}</span>
              </div>
            </div>
        </div>
    )
  }
}

function mapStateToProps(state){
  return {
    products: state.products,
    categories: state.categories,
    auth: state.auth
  }
}

export default withRouter(connect(
  mapStateToProps, 
  dispatch => ({
    getProducts: (data) => {
      dispatch(getProducts(data));
    },
    updateProductStatus: (data) => {
      dispatch(updateProductStatus(data));
    },
    updateProductViewStatus: (data) => {
      dispatch(updateProductViewStatus(data));
    },
    getCategories: (data) => {
      dispatch(getCategories(data));
    }
  })
)(MenuChange));