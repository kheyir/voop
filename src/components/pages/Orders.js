import React from 'react';
import { withRouter} from 'react-router-dom';
import {connect} from 'react-redux'
import ReactAudioPlayer from 'react-audio-player';
import config from '../../config'
import Countdown from 'react-countdown-now';
import Scripts from '../Scripts';
import {getIncomingOrders, getAcceptedOrders, getInDeliveryOrders, updateOrderStatus, updateOrderCookingTime, updateOrderAttention, emptyIncomingOrders, emptyAcceptedOrders, emptyInDeliveryOrders} from '../../actions/Orders'
import {pauseAndPlayAudio, playAudio, changeAudioMelody} from '../../actions/Audio'

export class Orders extends React.Component{

  constructor(props){
    super(props);

    this.eps = 5.1;
    this.incomingFirstTime = true;
    this.acceptedFirstTime = true;

    // this.audio = document.createElement("audio")

    this.state = {
      roadOrdersIsClosed: true,
      timeModalIsOpened: false,
      cancelModalIsOpened: false,
      viewModalIsOpened: false,
      forViewOrder: {},
      orderTypeForView: "",
      OrderIdForChangeTime: "",
      orderIdForCancel: ""
    }
  }

  componentWillMount(){
    console.log("componentWillMount")
    this.props.getIncomingOrders({
      authorization: this.props.auth.authorization
    })

    this.props.getAcceptedOrders({
      authorization: this.props.auth.authorization
    })

    this.props.getInDeliveryOrders({
      authorization: this.props.auth.authorization
    })

    this.forgottenOrders();
  }

  componentDidMount(){
    Scripts.ordersListBlocksHeightChange()
    Scripts.roadOrdersHeightChange();

    // this.audio.src = `music/${config.newOrderMelody}`
    // this.audio.loop = false;
  }

  componentWillUnmount(){
    console.log("componentWillUnmount")
    // this.props.emptyIncomingOrders();
    // this.props.emptyAcceptedOrders();
    // this.props.emptyInDeliveryOrders();
  }

  handleOnChange(name, value){

    var obj = Object.assign({}, this.state);

    obj[name] = value;

    this.setState(obj)
  }

  orderTimeChange(event, OrderIdForChangeTime){

    event.stopPropagation();

    this.setState({
      timeModalIsOpened: true,
      OrderIdForChangeTime: OrderIdForChangeTime,
      viewModalIsOpened: false,
      forViewOrder: {},
      orderTypeForView: ""
    })
  }

  orderCancelChange(event, orderIdForCancel){

    event.stopPropagation();

    this.setState({
      cancelModalIsOpened: true,
      orderIdForCancel: orderIdForCancel,
      viewModalIsOpened: false,
      forViewOrder: {},
      orderTypeForView: ""
    })
  }

  orderViewChange(event, orderType, orderIndex){

    event.stopPropagation();

    var order = JSON.parse(JSON.stringify(this.props[orderType].data[orderIndex]))

    this.setState({
      viewModalIsOpened: true,
      forViewOrder: order,
      orderTypeForView: orderType
    })
  }

  acceptOrder(orderId, cookingTime){
    this.props.updateOrderStatus({
      authorization: this.props.auth.authorization,
      orderId: orderId,
      cookingTime: cookingTime,
      status: 4
    })
  }

  toReadyOrder(event, orderId){

    event.stopPropagation();

    this.props.updateOrderStatus({
      authorization: this.props.auth.authorization,
      orderId: orderId,
      status: 6
    })

    this.setState({
      viewModalIsOpened: false,
      forViewOrder: {},
      orderTypeForView: ""
    })
  }

  cancelOrder(reason){
    this.props.updateOrderStatus({
      authorization: this.props.auth.authorization,
      orderId: this.state.orderIdForCancel,
      status: 5,
      cancel_reason: reason
    })
  }

  acceptOrderWithCookieTime(orderId, cookingTime){

    this.props.updateOrderStatus({
      authorization: this.props.auth.authorization,
      orderId: orderId,
      cookingTime: cookingTime,
      status: 4
    })
  }

  forgottenOrders(){
    console.log("acceptedOrders", this.props.acceptedOrders)
    if(this.props.incomingOrders.data){
      this.props.incomingOrders.data.map((item, index) => {
        this.props.updateOrderAttention({orderId: item.order_id, orderType: "INCOMING", attention: false})
      })
    }
    if(this.props.acceptedOrders.data){
      this.props.acceptedOrders.data.map((item, index) => {
        this.props.updateOrderAttention({orderId: item.order_id, orderType: "ACCEPTED", attention: false})
      })
    }

    this.forceUpdate();

    let now = new Date();


    if(this.props.incomingOrders.data){
      this.props.incomingOrders.data.map((item, index) => {

        let arrivalTime = new Date(item.order_arrival_time.split('-').join('/'));
        
        let diffOfDates = now - arrivalTime;

        if(diffOfDates >= this.props.profile.data.max_waiting_time * 60 * 1000){

          if(this.state.orderTypeForView == "incomingOrders" && item.order_id == this.state.forViewOrder.order_id){
            this.setState({
              viewModalIsOpened: false,
              forViewOrder: {},
              orderTypeForView: ""
            })
          }

          this.props.updateOrderStatus({
            authorization: this.props.auth.authorization,
            orderId: item.order_id,
            status: 5,
            cancel_reason: "time_is_over"
          })
        }
        else{
          if( diffOfDates % (60 * 1000) <= this.eps * 1000 && diffOfDates > (60 * 1000) && !this.props.newOrderModal.status){
            // console.log("sound")
            // this.props.pauseAndPlayAudio()
            this.props.audio.pause();
            this.props.audio.currentTime = 0;
            this.props.audio.src = `music/${config.newOrderMelody}`;
            Scripts.playSoundNTimes(this.props.audio, 3)
            this.props.updateOrderAttention({orderId: item.order_id, orderType: "INCOMING", attention: true})
          }
          else{
            // console.log("something else")
          }
        }

        if(index == this.props.incomingOrders.data.length - 1) this.incomingFirstTime = false;
      })
    }


    if(this.props.acceptedOrders.data){
      this.props.acceptedOrders.data.map((item, index) => {

        if(item.status == 4){
          let cookingExpTime = new Date(item.cooking_exp_time.split('-').join('/'));
          var acceptTime = (new Date(item.cooking_exp_time.split('-').join('/'))).setMinutes(cookingExpTime.getMinutes() - item.cooking_time)
          
          let diffOfDates = now - cookingExpTime;
  
          // if( diffOfDates > 0 && (diffOfDates % (60 * 1000)) <= (this.eps * 1000) && (now - acceptTime) >= (60 * 1000) || diffOfDates > 0 && this.acceptedFirstTime){
            if( diffOfDates > 0 && (diffOfDates % (60 * 1000)) <= (this.eps * 1000) || diffOfDates > 0 && this.acceptedFirstTime && !this.props.newOrderModal.status){
            // console.log("sound")
            // this.props.pauseAndPlayAudio()
            this.props.audio.pause();
            this.props.audio.currentTime = 0;
            this.props.audio.src = `music/${config.dishMelody}`;
            Scripts.playSoundNTimes(this.props.audio, 2)
            this.props.updateOrderAttention({orderId: item.order_id, orderType: "ACCEPTED", attention: true})
          }
          else{
            // console.log("something else")
          }
        }

        if(index == this.props.acceptedOrders.data.length - 1) this.acceptedFirstTime = false;
      })
    }

    setTimeout(() => {
      this.forgottenOrders()
    }, 5000);
  }

  

  render(){

    return(
      <div className="orders">
        <div className="orders-incoming">
          <h2 className={"orders-incoming__title" + (this.props.incomingOrders.data && this.props.incomingOrders.data.length > 0 && this.props.incomingOrders.data.filter(e => e.attention).length > 0 ? " orders-incoming__title_attention" : "")}>GƏLƏN ({this.props.incomingOrders.data && this.props.incomingOrders.data.length > 0 ? this.props.incomingOrders.data.length : '-'})</h2>
          <div className="orders-incoming-list-block">
            <ul className="orders-incoming-list">
              {
                this.props.incomingOrders.code == 200 && this.props.incomingOrders.data && this.props.incomingOrders.data.length > 0 && this.props.incomingOrders.data.map((incomingOrder, incomingOrderIndex) => (
                  <li className="orders-incoming-list__item">
                    <div onClick={(e) => {this.orderViewChange(e, "incomingOrders", incomingOrderIndex)}} className={"order" + (incomingOrder.attention ? " order-attention" : "")}>
                      <div className="order__body order__body_link">
                        <div className="order__left">
                          <div className="order__main-info">
                            <h1 className="order__user">{incomingOrder.user_name}</h1>
                            {
                              incomingOrder.user_is_new && (
                                <span className="order__mark order__mark_icon order__mark_icon_star">Yeni müştəri</span>
                              )
                            }
                          </div>
                          
                          {
                            incomingOrder.products && incomingOrder.products.length > 0 && (
                              <ul className="order-extra-list">
                                {
                                  incomingOrder.products.map(product => (
                                    <li className="order-extra-list__item">
                                      <div className="order-extra-list__info">
                                        <span className="order-extra-list__count">{product.quantity}</span>
                                        <span className="order-extra-list__name">
                                          {product.name}
                                          {
                                            product.extra_fields != null && product.extra_fields.length > 0 && (
                                              <span>:&nbsp; {product.extra_fields.map((extraFieldItem, extraFieldIndex) => extraFieldItem.name + (extraFieldIndex < product.extra_fields.length - 1 ? ', ' : ''))}</span>
                                            )
                                          }
                                        </span>
                                      </div>
                                      {
                                        product.user_note && (
                                          <span className="order__note">{product.user_note}</span>
                                        )
                                      }
                                    </li>
                                  ))
                                }
                              </ul>
                            )
                          }
                          
                          <span className="order__price">₼ {incomingOrder.order_price}</span>
                        </div>
                        <div className="order__right">
                          <span className="order__id">#{incomingOrder.order_id}</span>
                          <div className="order-object order-object_big">
                            <span className="order-object__name">Hazırlanma vaxtı:</span>
                            <span className="order-object__value">{incomingOrder.cooking_time} dəq.</span>
                          </div>
                        </div>
                      </div>
                      <div className="order__footer">
                        <button className="button button_bg_grey button_border-warm button_bor_rad_max button_pad_max_2 button_shadow mr-auto mr-5" onClick={(e) => {this.orderCancelChange(e, incomingOrder.order_id)}}>İMTİNA ET</button>
                        {/* <button className="button button_bg_white button_border-warm button_bor_rad_max button_pad_max_2 button_shadow ml-auto mr-5" onClick={this.orderTimeChange.bind(this, incomingOrder.order_id)}>VAXTI DƏYİŞ</button> */}
                        <button className="button button_bg_green-blue button_bor_rad_max button_pad_max_2 button_icon button_icon_ok button_shadow" onClick={(e) => {this.orderTimeChange(e, incomingOrder.order_id)}}>QƏBUL ET</button>
                      </div>
                    </div>
                  </li>
                ))
              }
            </ul>
            {
              this.props.incomingOrders.code == 201 && (
                <span className="error error_black">{this.props.incomingOrders.message}</span>
              )
            }
          </div>
        </div>
        <div className="orders-accepted">
          <h2 className={"orders-accepted__title" + (this.props.acceptedOrders.data && this.props.acceptedOrders.data.length > 0 && this.props.acceptedOrders.data.filter(e => e.attention).length > 0 ? " orders-accepted__title_attention" : "")}>QƏBUL EDİLƏN ({this.props.acceptedOrders.data && this.props.acceptedOrders.data.length > 0 ? this.props.acceptedOrders.data.length : '-'})</h2>

          <div className="orders-accepted-list-block">
            <ul className="orders-accepted-list">
              {
                this.props.acceptedOrders.code == 200 && this.props.acceptedOrders.data && this.props.acceptedOrders.data.length > 0 && this.props.acceptedOrders.data.map((acceptedOrder, acceptedOrderIndex) => (
                  <li className="orders-accepted-list__item">
                    <div onClick={(e) => {this.orderViewChange(e, "acceptedOrders", acceptedOrderIndex)}} className={"order" + (acceptedOrder.attention ? " order-attention" : "")}>
                      <div className="order__body">
                        <div className="order__left">
                          <div className="order__main-info">
                            <h1 className="order__user">{acceptedOrder.user_name}</h1>
                            {
                              acceptedOrder.user_is_new && (
                                <span className="order__mark order__mark_icon order__mark_icon_star">Yeni müştəri</span>
                              )
                            }
                          </div>
                          {
                            acceptedOrder.user_note && (
                              <span className="order__note">{acceptedOrder.user_note}</span>
                            )
                          }
                          
                          {
                            acceptedOrder.products && acceptedOrder.products.length > 0 && (
                              <ul className="order-extra-list">
                                {
                                  acceptedOrder.products.map(product => (
                                    <li className="order-extra-list__item">
                                      <div className="order-extra-list__info">
                                        <span className="order-extra-list__count">{product.quantity}</span>
                                        <span className="order-extra-list__name">
                                          {product.name}
                                          {
                                            product.extra_fields != null && product.extra_fields.length > 0 && (<span>:&nbsp;{product.extra_fields.map((extraFieldItem, extraFieldIndex) => extraFieldItem.name + (extraFieldIndex < product.extra_fields.length - 1 ? ', ' : ''))}</span>)
                                          }
                                        </span>
                                      </div>
                                      {
                                        product.user_note && (
                                          <span className="order__note">{product.user_note}</span>
                                        )
                                      }
                                    </li>
                                  ))
                                }
                              </ul>
                            )
                          }
                          
                          <span className="order__price">₼ {acceptedOrder.order_price}</span>
                        </div>
                        <div className="order__right">
                          <span className="order__id">#{acceptedOrder.order_id}</span>
                          <span className="order__add-text order__add-text_icon order__add-text_icon_motorcycle">Çatdırılma</span>
                          <div className="order-object">
                            <span className="order-object__name">{acceptedOrder.courier_name}</span>
                            {
                              acceptedOrder.arriveTime && (
                                <span className="order-object__value">{acceptedOrder.arriveTime} deq.</span>
                              )
                            }
                          </div>
                        </div>
                      </div>
                      <div className="order__footer">
                        {
                          acceptedOrder.status == 6 ? (
                            <span className="c-green-blue font-semi-bold pt-5 pb-5">Sifariş hazırdır, kuryer gözlənilir</span>
                          ) : (
                            <div className="flex-row-center-between w-100">
                              <div className="order-delivery-time">
                                <span className="order-delivery-time__time">
                                  <Countdown 
                                    date={new Date(acceptedOrder.cooking_exp_time.split('-').join('/'))}
                                    renderer={
                                      ({ hours, minutes, seconds, completed }) => {
                                        if (completed) {
                                          return "00:00";
                                        } else {
                                          return <span>{minutes}:{seconds}</span>;
                                        }
                                      }
                                    }
                                  />
                                </span>
                                <span className="order-delivery-time__time-lost">/ {acceptedOrder.cooking_time} dəq.</span>
                              </div>
                              <button className="button button_bg_green-blue button_bor_rad_max button_pad_max_2 button_icon button_icon_2_arrows button_shadow ml-auto" onClick={(e) => {this.toReadyOrder(e, acceptedOrder.order_id)}}>SİFARİŞ HAZIRDIR</button>
                            </div>
                          )
                        }
                        
                      </div>
                    </div>
                  </li>
                ))
              }
            </ul>
            {
              this.props.acceptedOrders.code == 201 && (
                <span className="error error_black">{this.props.acceptedOrders.message}</span>
              )
            }

            <div className={`orders-road${this.state.roadOrdersIsClosed ? " orders-road_closed" : ""}`}>
              <div className="orders-road__header" onClick={this.handleOnChange.bind(this, "roadOrdersIsClosed", !this.state.roadOrdersIsClosed)}>
                <h2 className="orders-road__title">YOLDA OLAN SİFARİŞLƏR ({this.props.inDeliveryOrders.data && this.props.inDeliveryOrders.data.length > 0 ? this.props.inDeliveryOrders.data.length : '-'})</h2>
              </div>
              <div className="orders-road__body">
                <ul className="orders-road-list">
                  {
                    this.props.inDeliveryOrders.code == 200 && this.props.inDeliveryOrders.data && this.props.inDeliveryOrders.data.length > 0 && this.props.inDeliveryOrders.data.map(inDeliveryOrder => (
                      <li className="orders-road-list__item">
                        <div className="order">
                          <div className="order__body">
                            <div className="order__left">
                              <div className="order__main-info">
                                <h1 className="order__user">{inDeliveryOrder.user_name}</h1>
                                {
                                  inDeliveryOrder.user_is_new && (
                                    <span className="order__mark order__mark_icon order__mark_icon_star">Yeni müştəri</span>
                                  )
                                }
                              </div>
                              {
                                inDeliveryOrder.user_note && (
                                  <span className="order__note">{inDeliveryOrder.user_note}</span>
                                )
                              }
                              
                              {
                                inDeliveryOrder.products && inDeliveryOrder.products.length > 0 && (
                                  <ul className="order-extra-list">
                                    {
                                      inDeliveryOrder.products.map(product => (
                                        <li className="order-extra-list__item">
                                          <div className="order-extra-list__info">
                                            <span className="order-extra-list__count">{product.quantity}</span>
                                            <span className="order-extra-list__name">{product.name}</span>
                                          </div>
                                        </li>
                                      ))
                                    }
                                  </ul>
                                )
                              }
                              
                              <span className="order__price">₼ {inDeliveryOrder.order_price}</span>
                            </div>
                            <div className="order__right">
                              <span className="order__id">#{inDeliveryOrder.order_id}</span>
                              <span className="order__add-text order__add-text_icon order__add-text_icon_motorcycle">Çatdırılma</span>
                            </div>
                          </div>
                        </div>
                      </li>
                    ))
                  }
                </ul>
                {
                  this.props.inDeliveryOrders.code == 201 && (
                    <span className="error error_black">{this.props.inDeliveryOrders.message}</span>
                  )
                }
              </div>
            </div>
          </div>
        </div>
      
      {
        this.state.timeModalIsOpened && (
          <div className="time-modal-block" onClick={this.handleOnChange.bind(this, "timeModalIsOpened", false)}>
            <div className="time-modal" onClick={e => {e.stopPropagation()}}>
              <div className="time-modal__header">
                <h3 className="time-modal__title">Vaxtı seçin (dəq.) və sifarişi qəbul edin</h3>
                <a href="javascript:void(0)" className="time-modal__close" onClick={this.handleOnChange.bind(this, "timeModalIsOpened", false)}></a>
              </div>
              <div className="time-modal__body">
                <ul className="time-modal-minutes-list">
                  <li className="time-modal-minutes-list__item">
                    <a href="javascript:void(0)" className="time-modal-minutes-list__link" onClick={() => {this.acceptOrderWithCookieTime(this.state.OrderIdForChangeTime, 5); this.handleOnChange("timeModalIsOpened", false)}}>05</a>
                  </li>
                  <li className="time-modal-minutes-list__item">
                    <a href="javascript:void(0)" className="time-modal-minutes-list__link" onClick={() => {this.acceptOrderWithCookieTime(this.state.OrderIdForChangeTime, 10); this.handleOnChange("timeModalIsOpened", false)}}>10</a>
                  </li>
                  <li className="time-modal-minutes-list__item">
                    <a href="javascript:void(0)" className="time-modal-minutes-list__link" onClick={() => {this.acceptOrderWithCookieTime(this.state.OrderIdForChangeTime, 15); this.handleOnChange("timeModalIsOpened", false)}}>15</a>
                  </li>
                  <li className="time-modal-minutes-list__item">
                    <a href="javascript:void(0)" className="time-modal-minutes-list__link" onClick={() => {this.acceptOrderWithCookieTime(this.state.OrderIdForChangeTime, 20); this.handleOnChange("timeModalIsOpened", false)}}>20</a>
                  </li>
                  <li className="time-modal-minutes-list__item">
                    <a href="javascript:void(0)" className="time-modal-minutes-list__link" onClick={() => {this.acceptOrderWithCookieTime(this.state.OrderIdForChangeTime, 25); this.handleOnChange("timeModalIsOpened", false)}}>25</a>
                  </li>
                  <li className="time-modal-minutes-list__item">
                    <a href="javascript:void(0)" className="time-modal-minutes-list__link" onClick={() => {this.acceptOrderWithCookieTime(this.state.OrderIdForChangeTime, 30); this.handleOnChange("timeModalIsOpened", false)}}>30</a>
                  </li>
                  <li className="time-modal-minutes-list__item">
                    <a href="javascript:void(0)" className="time-modal-minutes-list__link" onClick={() => {this.acceptOrderWithCookieTime(this.state.OrderIdForChangeTime, 35); this.handleOnChange("timeModalIsOpened", false)}}>35</a>
                  </li>
                  <li className="time-modal-minutes-list__item">
                    <a href="javascript:void(0)" className="time-modal-minutes-list__link" onClick={() => {this.acceptOrderWithCookieTime(this.state.OrderIdForChangeTime, 40); this.handleOnChange("timeModalIsOpened", false)}}>40</a>
                  </li>
                  <li className="time-modal-minutes-list__item">
                    <a href="javascript:void(0)" className="time-modal-minutes-list__link" onClick={() => {this.acceptOrderWithCookieTime(this.state.OrderIdForChangeTime, 45); this.handleOnChange("timeModalIsOpened", false)}}>45</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        )
      }
      
      {/* {
        this.state.timeModalIsOpened && (
          <div className="time-modal-block" onClick={this.handleOnChange.bind(this, "timeModalIsOpened", false)}>
            <div className="time-modal" onClick={e => {e.stopPropagation()}}>
              <div className="time-modal__header">
                <h3 className="time-modal__title">Vaxtı seçin (dəq.) və sifarişi qəbul edin</h3>
                <a href="javascript:void(0)" className="time-modal__close" onClick={this.handleOnChange.bind(this, "timeModalIsOpened", false)}></a>
              </div>
              <div className="time-modal__body">
                <ul className="time-modal-minutes-list">
                  <li className="time-modal-minutes-list__item">
                    <a href="javascript:void(0)" className="time-modal-minutes-list__link" onClick={() => {this.props.updateOrderCookingTime({orderId: this.state.OrderIdForChangeTime, cookingTime: 5}); this.handleOnChange("timeModalIsOpened", false)}}>05</a>
                  </li>
                  <li className="time-modal-minutes-list__item">
                    <a href="javascript:void(0)" className="time-modal-minutes-list__link" onClick={() => {this.props.updateOrderCookingTime({orderId: this.state.OrderIdForChangeTime, cookingTime: 10}); this.handleOnChange("timeModalIsOpened", false)}}>10</a>
                  </li>
                  <li className="time-modal-minutes-list__item">
                    <a href="javascript:void(0)" className="time-modal-minutes-list__link" onClick={() => {this.props.updateOrderCookingTime({orderId: this.state.OrderIdForChangeTime, cookingTime: 15}); this.handleOnChange("timeModalIsOpened", false)}}>15</a>
                  </li>
                  <li className="time-modal-minutes-list__item">
                    <a href="javascript:void(0)" className="time-modal-minutes-list__link" onClick={() => {this.props.updateOrderCookingTime({orderId: this.state.OrderIdForChangeTime, cookingTime: 20}); this.handleOnChange("timeModalIsOpened", false)}}>20</a>
                  </li>
                  <li className="time-modal-minutes-list__item">
                    <a href="javascript:void(0)" className="time-modal-minutes-list__link" onClick={() => {this.props.updateOrderCookingTime({orderId: this.state.OrderIdForChangeTime, cookingTime: 25}); this.handleOnChange("timeModalIsOpened", false)}}>25</a>
                  </li>
                  <li className="time-modal-minutes-list__item">
                    <a href="javascript:void(0)" className="time-modal-minutes-list__link" onClick={() => {this.props.updateOrderCookingTime({orderId: this.state.OrderIdForChangeTime, cookingTime: 30}); this.handleOnChange("timeModalIsOpened", false)}}>30</a>
                  </li>
                  <li className="time-modal-minutes-list__item">
                    <a href="javascript:void(0)" className="time-modal-minutes-list__link" onClick={() => {this.props.updateOrderCookingTime({orderId: this.state.OrderIdForChangeTime, cookingTime: 35}); this.handleOnChange("timeModalIsOpened", false)}}>35</a>
                  </li>
                  <li className="time-modal-minutes-list__item">
                    <a href="javascript:void(0)" className="time-modal-minutes-list__link" onClick={() => {this.props.updateOrderCookingTime({orderId: this.state.OrderIdForChangeTime, cookingTime: 40}); this.handleOnChange("timeModalIsOpened", false)}}>40</a>
                  </li>
                  <li className="time-modal-minutes-list__item">
                    <a href="javascript:void(0)" className="time-modal-minutes-list__link" onClick={() => {this.props.updateOrderCookingTime({orderId: this.state.OrderIdForChangeTime, cookingTime: 45}); this.handleOnChange("timeModalIsOpened", false)}}>45</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        )
      } */}

        {
          this.state.viewModalIsOpened && (
            <div className="view-modal-block" onClick={this.handleOnChange.bind(this, "viewModalIsOpened", false)}>
              <div className="view-modal" onClick={e => {e.stopPropagation()}}>
                <div className="view-modal__header">
                  <a href="javascript:void(0)" className="view-modal__close" onClick={this.handleOnChange.bind(this, "viewModalIsOpened", false)}></a>
                </div>
                <div className="view-modal__body">

                  <div className="order">
                    <div className="order__body">
                      <div className="order__left">
                        <div className="order__main-info">
                          <h1 className="order__user">{this.state.forViewOrder.user_name}</h1>
                          {
                            this.state.forViewOrder.user_is_new && (
                              <span className="order__mark order__mark_icon order__mark_icon_star">Yeni müştəri</span>
                            )
                          }
                        </div>
                        
                        {
                          this.state.forViewOrder.products && this.state.forViewOrder.products.length > 0 && (
                            <ul className="order-extra-list">
                              {
                                this.state.forViewOrder.products.map(product => (
                                  <li className="order-extra-list__item">
                                    <div className="order-extra-list__info">
                                      <span className="order-extra-list__count">{product.quantity}</span>
                                      <span className="order-extra-list__name">
                                        {product.name}
                                        {
                                          product.extra_fields != null && product.extra_fields.length > 0 && (
                                            <span>:&nbsp; {product.extra_fields.map((extraFieldItem, extraFieldIndex) => extraFieldItem.name + (extraFieldIndex < product.extra_fields.length - 1 ? ', ' : ''))}</span>
                                          )
                                        }
                                      </span>
                                    </div>
                                    {
                                      product.user_note && (
                                        <span className="order__note">{product.user_note}</span>
                                      )
                                    }
                                  </li>
                                ))
                              }
                            </ul>
                          )
                        }
                        
                        <span className="order__price">₼ {this.state.forViewOrder.order_price}</span>
                      </div>
                      <div className="order__right">
                        <span className="order__id">#{this.state.forViewOrder.order_id}</span>

                        {
                          this.state.orderTypeForView == "incomingOrders" && (
                            <div className="order-object order-object_big">
                              <span className="order-object__name">Hazırlanma vaxtı:</span>
                              <span className="order-object__value">{this.state.forViewOrder.cooking_time} dəq.</span>
                            </div>
                          ) 
                        }

                        {
                          this.state.orderTypeForView == "acceptedOrders" && (
                            <span className="order__add-text order__add-text_icon order__add-text_icon_motorcycle">Çatdırılma</span>
                          ) 
                        }

                        {
                          this.state.orderTypeForView == "acceptedOrders" && (
                            <div className="order-object">
                              <span className="order-object__name">{this.state.forViewOrder.courier_name}</span>
                              {
                                this.state.forViewOrder.arriveTime && (
                                  <span className="order-object__value">{this.state.forViewOrder.arriveTime} deq.</span>
                                )
                              }
                            </div>
                          ) 
                        }
                        

                        
                        
                      </div>
                    </div>
                    <div className="order__footer">
                      {
                        this.state.orderTypeForView == "incomingOrders" && (
                          <button className="button button_bg_grey button_border-warm button_bor_rad_max button_pad_max_2 button_shadow mr-auto mr-5" onClick={(e) => {this.orderCancelChange(e, this.state.forViewOrder.order_id)}}>İMTİNA ET</button>
                        ) 
                      }
                      {
                        this.state.orderTypeForView == "incomingOrders" && (
                          <button className="button button_bg_green-blue button_bor_rad_max button_pad_max_2 button_icon button_icon_ok button_shadow" onClick={(e) => {this.orderTimeChange(e, this.state.forViewOrder.order_id)}}>QƏBUL ET</button>
                        )
                      }

                      {
                        this.state.orderTypeForView == "acceptedOrders" && this.state.forViewOrder.status == 6 && (
                          <span className="c-green-blue font-semi-bold pt-5 pb-5">Sifariş hazırdır, kuryer gözlənilir</span>
                        )
                      }

                      {
                        this.state.orderTypeForView == "acceptedOrders" && this.state.forViewOrder.status != 6 && (
                          <div className="flex-row-center-between w-100">
                            <div className="order-delivery-time">
                              <span className="order-delivery-time__time">
                                <Countdown 
                                  date={new Date(this.state.forViewOrder.cooking_exp_time.split('-').join('/'))}
                                  renderer={
                                    ({ hours, minutes, seconds, completed }) => {
                                      if (completed) {
                                        return "00:00";
                                      } else {
                                        return <span>{minutes}:{seconds}</span>;
                                      }
                                    }
                                  }
                                />
                              </span>
                              <span className="order-delivery-time__time-lost">/ {this.state.forViewOrder.cooking_time} dəq.</span>
                            </div>
                            <button className="button button_bg_green-blue button_bor_rad_max button_pad_max_2 button_icon button_icon_2_arrows button_shadow ml-auto" onClick={(e) => {this.toReadyOrder(e, this.state.forViewOrder.order_id)}}>SİFARİŞ HAZIRDIR</button>
                          </div>
                        )
                      }
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )
        }

        {
          this.state.cancelModalIsOpened && (
            <div className="cancel-modal-block" onClick={this.handleOnChange.bind(this, "cancelModalIsOpened", false)}>
              <div className="cancel-modal" onClick={e => {e.stopPropagation()}}>
                <div className="cancel-modal__header">
                  <h3 className="cancel-modal__title">İmtina səbəbini seçin</h3>
                  <span className="cancel-modal__mini-description">İmtina səbəbi müştəriyə göndəriləcəkdir</span>
                  <a href="javascript:void(0)" className="cancel-modal__close" onClick={this.handleOnChange.bind(this, "cancelModalIsOpened", false)}></a>
                </div>
                <div className="cancel-modal__body">
                  <ul className="cancel-modal-reason-list">
                    <li className="cancel-modal-reason-list__item">
                      <a href="javascript:void(0)" className="cancel-reason" onClick={e => {this.cancelOrder("item_unavailable"); this.handleOnChange("cancelModalIsOpened", false)}}>
                        <img className="cancel-reason__img" src="images/icon_french_fries.svg" />
                        <span className="cancel-reason__title">Məhsul (lar) mövcud deyil</span>
                        <span className="cancel-reason__description">Təəssüf ki, sifariş etdiyiniz məhsullar bitmişdir.</span>
                      </a>
                    </li>
                    <li className="cancel-modal-reason-list__item">
                      <a href="javascript:void(0)" className="cancel-reason" onClick={e => {this.cancelOrder("closing_soon"); this.handleOnChange("cancelModalIsOpened", false)}}>
                        <img className="cancel-reason__img" src="images/icon_clock_close.svg" />
                        <span className="cancel-reason__title">Tezliklə bağlanırıq</span>
                        <span className="cancel-reason__description">Təəssüf ki, sifariş etdiyiniz məhsulları hazırlaya bilmirik.</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          )
        }
        

      </div>
    )
  }
}

function mapStateToProps(state){
  return {
    auth: state.auth,
    incomingOrders: state.incomingOrders,
    acceptedOrders: state.acceptedOrders,
    inDeliveryOrders: state.inDeliveryOrders,
    profile: state.profile,
    audio: state.audio,
    newOrderModal: state.newOrderModal
  }
}

export default withRouter(connect(
  mapStateToProps, 
  dispatch => ({
    getIncomingOrders: (data) => {
      dispatch(getIncomingOrders(data));
    },
    getAcceptedOrders: (data) => {
      dispatch(getAcceptedOrders(data));
    },
    getInDeliveryOrders: (data) => {
      dispatch(getInDeliveryOrders(data));
    },
    updateOrderStatus: (data) => {
      dispatch(updateOrderStatus(data));
    },
    updateOrderCookingTime: (data) => {
      dispatch(updateOrderCookingTime(data));
    },
    updateOrderAttention: (data) => {
      dispatch(updateOrderAttention(data));
    },
    pauseAndPlayAudio: () => {
      dispatch(pauseAndPlayAudio());
    },
    playAudio: () => {
      dispatch(playAudio());
    },
    changeAudioMelody: (data) => {
      dispatch(changeAudioMelody(data));
    },
    emptyIncomingOrders: () => {
      dispatch(emptyIncomingOrders());
    },
    emptyAcceptedOrders: () => {
      dispatch(emptyAcceptedOrders());
    },
    emptyInDeliveryOrders: () => {
      dispatch(emptyInDeliveryOrders());
    }
  })
)(Orders));