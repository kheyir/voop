import React from 'react';
import Scripts from '../Scripts';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux'
import {getVideos} from '../../actions/Videos';

export class Videos extends React.Component{

  constructor(props){
    super(props);
  }

  componentWillMount(){
    this.props.getVideos({
        authorization: this.props.auth.authorization
    })
  }

  componentDidMount(){
    Scripts.videosHeightChange()
  }
  
  render(){

    return(
        <div className="videos">
            <div className="videos-head mt-20">
                <h1 className="videos__title">Ping Pong Delivery - merchant tutorial videos</h1>
                <span className="videos__descriprion">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes</span>
            </div>

            <ul className="videos-list ml-20">
                {
                    this.props.videos.data && this.props.videos.data.length > 0 && this.props.videos.data.map(videoItem => (
                        <li className="videos-list__item">
                            <a href={videoItem.link} className="videos-list__link">
                                <div className="videos-list__img-box">
                                    <div className="videos-list__img" style={{backgroundImage: "url('"+videoItem.preview+"')"}}></div>
                                    {/* <span className="videos-list__time">1:58</span> */}
                                </div>
                                <div className="videos-list__right">
                                    <h2 className="videos-list__title">{videoItem.video_title}</h2>
                                    {/* <div className="videos-list__description-block">
                                        <span className="videos-list__channel">Ping Pong</span>
                                        <span className="videos-list__duration">1,6 min baxış</span>
                                    </div> */}
                                </div>
                            </a>
                        </li>
                    ))
                }
            </ul>
        </div>
    )
  }
}

function mapStateToProps(state){
  return {
    auth: state.auth,
    videos: state.videos
  }
}

export default withRouter(connect(
  mapStateToProps, 
  dispatch => ({
    getVideos: (data) => {
      dispatch(getVideos(data));
    }
  })
)(Videos));