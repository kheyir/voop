const createMemoryHistory = require('history/createMemoryHistory').default

const history = createMemoryHistory()

// Get the current location.
const location = history.location

// const unlisten = history.listen((location, action) => {
//   console.log(action, location.pathname, location.state)
// })

// Use push, replace, and go to navigate around.
history.push("/home", { some: "state" })

// To stop listening, call the function returned from listen().
//unlisten()

export default history;