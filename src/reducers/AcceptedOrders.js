export default function(state = {data: []}, action){

    switch(action.type){
        case "GET_ACCEPTED_ORDERS":
            state = action.data;
            break;
        case "UPDATE_ACCEPTED_ORDERS_COURIER":
            var obj = Object.assign({}, state)
            obj.data.forEach((acceptedOrderItem, acceptedOrderIndex) => {
                if(acceptedOrderItem.order_id == action.data.orderId){
                    obj.data[acceptedOrderIndex].courier_name = action.data.courierName
                    obj.data[acceptedOrderIndex].arriveTime = action.data.arriveTime
                }
            });

            state = Object.assign({}, obj);
            break;
        case "UPDATE_ACCEPTED_ORDERS_STATUS":
            var obj = Object.assign({}, state)
            obj.data.forEach((acceptedOrderItem, acceptedOrderIndex) => {
                if(acceptedOrderItem.order_id == action.data.orderId){
                    obj.data[acceptedOrderIndex].status = action.data.status
                }
            });

            state = Object.assign({}, obj);
            break;
        case "UPDATE_ACCEPTED_ORDERS_ATTENTION":
            var obj = Object.assign({}, state)

            obj.data.forEach((incomingOrderItem, incomingOrderIndex) => {
                if(incomingOrderItem.order_id == action.data.orderId){
                    obj.data[incomingOrderIndex].attention = action.data.attention
                }
            });
            
            state = Object.assign({}, obj);
            break;
        case "EMPTY_ACCEPTED_ORDERS":
            state = Object.assign({}, {data: []})
            break;
        
    }

    return state
}