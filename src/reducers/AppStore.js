import {combineReducers} from 'redux';

import Auth from './Auth'
import Loader from './Loader'

import Videos from './Videos';
import Products from './Products';
import Categories from './Categories';
import History from './History';
import Profile from './Profile';
import NewOrderModal from './NewOrderModal';
import IncomingOrders from './IncomingOrders';
import AcceptedOrders from './AcceptedOrders';
import InDeliveryOrders from './InDeliveryOrders';
import LineStatus from './LineStatus';
import RushTimeStatus from './RushTimeStatus';
import MiniModal from './MiniModal';
import RightNowIncomingOrders from './RightNowIncomingOrders';
import Audio from './Audio';

const AppStore = combineReducers({

    auth: Auth,
    loader: Loader,
    miniModal: MiniModal,
    lineStatus: LineStatus,
    rushTimeStatus: RushTimeStatus,

    videos: Videos,
    products: Products,
    categories: Categories,
    history: History,
    profile: Profile,
    newOrderModal: NewOrderModal,
    incomingOrders: IncomingOrders,
    acceptedOrders: AcceptedOrders,
    inDeliveryOrders: InDeliveryOrders,
    rightNowIncomingOrders: RightNowIncomingOrders,
    audio: Audio

})

export default AppStore;