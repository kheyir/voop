import config from '../config'
import Scripts from '../components/Scripts'

export default function(state = {}, action){

    switch(action.type){
        case "INIT_AUDIO":
            var audio = document.createElement("audio")
            audio.src = `music/${config.newOrderMelody}`
            audio.loop = false;

            state = audio;
            break;
        case "PAUSE_AND_PLAY_AUDIO":
            var audio = state;
            audio.pause();
            audio.currentTime = 0;
            Scripts.playSoundNTimes(audio, 3)
            state = audio;
            break;
        case "PLAY_AUDIO":
            var audio = state;
            Scripts.playSoundNTimes(audio, 3)
            break;
    }

    return state
}