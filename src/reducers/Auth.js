import Cookies from 'universal-cookie';

export default function(state = {signed: 0, authorization: ""}, action){

    var obj = Object.assign({}, state);

    switch(action.type){
        case "SIGNED_IN":
            obj.signed = 1;
            obj.authorization = action.authorization;
            obj.code = action.code;
            obj.message = action.message;
            var oneYear = new Date();
            oneYear.setDate(new Date().getDate()+365);
            (new Cookies()).set('authorization', action.authorization, { path: '/', secure: false, expires: oneYear });
            break;
        case "SIGNED_OUT":
            obj.signed = 0;
            obj.authorization = "";
            obj.code = action.code;
            obj.message = action.message;
            (new Cookies()).remove('authorization', { path: '/' });
            if(window.location.pathname != "/partner/login") window.location.href = "/partner/login"
            break;
    }

    state = Object.assign({}, state, obj);

    return state
}