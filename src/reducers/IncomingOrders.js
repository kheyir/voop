export default function(state = {data: []}, action){

    switch(action.type){
        case "GET_INCOMING_ORDERS":
            state = action.data;
            break;
        case "UPDATE_INCOMING_ORDERS_COOKING_TIME":
            var obj = Object.assign({}, state)

            obj.data.forEach((incomingOrderItem, incomingOrderIndex) => {
                if(incomingOrderItem.order_id == action.data.orderId){
                    obj.data[incomingOrderIndex].cooking_time = action.data.cookingTime
                }
            });
            
            state = Object.assign({}, obj);
            break;
        case "UPDATE_INCOMING_ORDERS_ATTENTION":
            var obj = Object.assign({}, state)

            obj.data.forEach((incomingOrderItem, incomingOrderIndex) => {
                if(incomingOrderItem.order_id == action.data.orderId){
                    obj.data[incomingOrderIndex].attention = action.data.attention
                }
            });
            
            state = Object.assign({}, obj);
            break;
        case "EMPTY_INCOMING_ORDERS":
            state = {data: []};
            break;
        
    }

    return state
}