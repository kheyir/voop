import config from '../config'

export default function(state = true, action){

    switch(action.type){
        case "ACTIVE_LINE_STATUS":
            state = true
            break;
        case "DEACTIVE_LINE_STATUS":
            state = false
            break;
    }

    return state
}