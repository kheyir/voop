import config from '../config'

export default function(state = false, action){

    switch(action.type){
        case "ACTIVE_LOADER":
            state = true
            break;
        case "DEACTIVE_LOADER":
            state = false
            break;
    }

    return state
}