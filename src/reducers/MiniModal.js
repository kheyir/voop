import config from '../config'

export default function(state = {status: false, text: ""}, action){

    switch(action.type){
        case "ACTIVE_MINIMODAL":
            var newState = {
                status: true,
                text: action.text
            }
            state = Object.assign({}, newState)
            break;
        case "DEACTIVE_MINIMODAL":
            var newState = {
                status: false,
                text: ""
            }
            state = Object.assign({}, newState)
            break;
    }

    return state
}