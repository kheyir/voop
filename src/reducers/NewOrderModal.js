import config from '../config'

export default function(state = {status: false, inComingCount: 0}, action){

    switch(action.type){
        case "ACTIVE_NEW_ORDER_MODAL":
            var obj = Object.assign({}, state)

            obj.status = action.data.status;
            obj.inComingCount += action.data.inComingCount;

            state = action.data
            break;
        case "DEACTIVE_NEW_ORDER_MODAL":
            state = {status: false, inComingCount: 0}
            break;
    }

    return state
}