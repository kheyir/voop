import config from '../config'

export default function(state = {data: []}, action){

    switch(action.type){
        case "GET_PRODUCTS":
            state = action.data;
            break;
        case "UPDATE_PRODUCT_STATUS":
            var tmpState = Object.assign({}, state)

            tmpState.data.forEach((productItem, productIndex) => {
                if(productItem.product_id == action.data.productId){
                    if(!action.data.hasOwnProperty("extraFieldKey")){
                        tmpState.data[productIndex].status = action.data.status == "enable" ? true : false
                    }
                    else{
                        productItem.extra_fields.forEach((extraFieldItem, extraFieldIndex) => {
                            if(extraFieldItem.extra_field_key == action.data.extraFieldKey){
                                tmpState.data[productIndex].extra_fields[extraFieldIndex].status = action.data.status == "enable" ? true : false
                            }
                        })
                    }
                }
            });
            state = Object.assign({}, tmpState)
            break;
        case "UPDATE_PRODUCT_VIEW_STATUS":
            var tmpState = Object.assign({}, state)
            tmpState.data[action.data.productIndex].extraFieldsShow = action.data.status
            state = Object.assign({}, tmpState)
            break;
        case "EMPTY_PRODUCTS":
            state = {data: []};
            break;
        
    }

    return state
}