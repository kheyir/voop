export default function(state = {data: []}, action){

    switch(action.type){
        case "GET_RIGHT_NOW_INCOMING_ORDERS":
            state = action.data;
            break;
        case "ADD_RIGHT_NOW_INCOMING_ORDERS":
            var obj = Object.assign({}, state)

            obj.data.push(action.data);
            
            state = Object.assign({}, obj);
            break;
        case "REMOVE_ONE_RIGHT_NOW_INCOMING_ORDER":
            // var obj = Object.assign({}, state)

            state.data = state.data.filter((item, index) => {
                return item.orderId != action.data.orderId
            });

            // obj.data = newArray;
            
            // state = Object.assign({}, obj);
            // console.log("removed: " + action.data.orderId + "on", state)
            break;
        case "EMPTY_RIGHT_NOW_INCOMING_ORDERS":
            state = {data: []};
            break;
    }

    return state
}