import config from '../config'

export default function(state = false, action){

    switch(action.type){
        case "ACTIVE_RUSH_TIME_STATUS":
            state = true
            break;
        case "DEACTIVE_RUSH_TIME_STATUS":
            state = false
            break;
    }

    return state
}