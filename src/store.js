//import browserHistory from 'history'
import {createStore, applyMiddleware} from 'redux';
//import { push, routerMiddleware } from 'react-router-redux'

import thunk from 'redux-thunk'

import AppStore from './reducers/AppStore';

export default createStore(AppStore, applyMiddleware(thunk))
